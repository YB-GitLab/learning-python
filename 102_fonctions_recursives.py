# Fonctions Récursives
"""
def a(n, limit):
    if n > limit:
        return
    print("n =", n)
    a(n * n, limit)


a(2, 100000)
"""


def demander_choix_utilisateur(min, max):
    reponse_str = input("Quel est votre choix entre " + str(min) + " et " + str(max) + " : ")
    try:
        reponse_int = int(reponse_str)
        if not min <= reponse_int <= max:
            print(f"ERRREUR, entrez un nombre entre {min} et {max}")
            return demander_choix_utilisateur(min, max)  # condition de sortie invalide alors on reboucle sur la fonction
        return reponse_int  # Condition de sortie valide alors on sort de la boucle
    except:
        print("ERREUR: Vous devez rentrer un rombre")
        return demander_choix_utilisateur(min, max)  # condition de sortie invalide alors on reboucle sur la fonction


choix = demander_choix_utilisateur(1, 4)
print("Choix de l'utilisateur :", choix)