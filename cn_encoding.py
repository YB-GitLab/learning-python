import sys
import math

# Modifier le message en valeur binaire


def get_binary_from_char(c):
    return '{0:07b}'.format(ord(c))


def get_binary_from_str(s):
    result = ""
    for c in s:
        result += get_binary_from_char(c)
    return result


def generate_cn_encoding_for_sequence(c, count):
    s = ""
    if c == "1":
        s = "0 "
    else:
        s = "00 "
    s += "0" * count
    return s


def generate_cn_encoding(b):
    c = b[0]  # on commence par lire le premier caractère
    count = 1
    seq = ""
    for i in range(1, len(b)):
        if b[i] == c:  # ici on compare le nouveau caractère avec le caractère précédent, si ils sont identitiques on incrémente le count
            count += 1
        else:  # ici si le caractère n'est pas identique, on «flush» la séquence pour commencer une nouvelle
            # print("Sequence", c, count)  # On affiche les séquences comme «sequence 1 3» «sequence 0 2» etc…
            if len(seq) > 0:
                seq += " "
            seq += generate_cn_encoding_for_sequence(c, count)
            c = b[i]
            count = 1
    if len(seq) > 0:
        seq += " "
    seq += generate_cn_encoding_for_sequence(c, count)
    return seq
    # print("sequence finale", c, count)  # on fini par une séquence finale




binary = get_binary_from_str("CC")  # ici on retourne le résultat de get_binary_from_str() qui en même temps fait mouliner le get_binary_from_char
print(binary)
print(generate_cn_encoding(binary))  # ici on récupère la valeur de binary pour l'injecter dans generate_cn_encoding

# for each in binary_converted:
#     count = 0
#     if each == 1:
#         count += 1
#     else:
