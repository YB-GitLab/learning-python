# La négation unaire

a = 10
b = 3.5
c = -2
d = -4.7

print(-a)
print(-b)
print(-c)
print(-d)

print('------------')

a = 10
b = 5

c = -a * -b

print(c)

# L'exponentiation
print('-------------')
a = 4
print(a**0.5)
print(a**2)
print(a**3)