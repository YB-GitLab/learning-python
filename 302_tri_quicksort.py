# tri rapide : Quick sort
# arr = [8, 3, 7, 9, 1, 4]
import random

# [8, 3, 7, 9, 1, 4]  # 4 sera le pivot
# ....   4   ....     # tout ce qui est inférieur au pivot sera à gauche de celui-ci et les supérieurs à droite
# 3, 1   4  8, 7, 9

# [8, 3, 7, 9, 1, 4] # p = 4
# [3, 1, 4, 8, 7, 9] # a = 2 petits éléments (3 et 1)

# [3, 1] # p = 1
# [1, 3] # a = 0

# [8, 7, 9] # p = 9  # comme le pivot est 9, la liste ne change pas mais a = 2 (petits éléments)
# [8, 7, 9] # a = 2
# [8, 7]
# [7, 8]

# [1, 3] 4 [7, 8] 9 # reconstitution de la liste final

def generate_random_list(n, min, max):
    l = []
    for i in range(n):
        e = random.randint(min, max)
        l.append(e)

    return l

def quicksort(l):
    qsort_loop(l, 0, len(l)-1)

def qsort_loop(l, imin, imax):
    if imax-imin == 1:
        if l[imin] > l[imax]:
            """c = l[imin]      #
            l[imin] = l[imax]   #version qui fonctionne pour tous les langages
            l[imax] = c"""      #
            l[imin], l[imax] = l[imax], l[imin]  # version courte de python
        return
    if imax-imin == 0:
        return

    p = l[imax]
    a = 0
    for i in range(imin, imax):
        if l[i] <= p:
            l[a+imin], l[i] = l[i], l[a+imin]
            a += 1
    l[a+imin], l[imax] = p, l[a+imin]
    if a != 0:
        qsort_loop(l, imin, a+imin-1)
    if imax > a+imin+1:
        qsort_loop(l, a+imin+1, imax)


def check_ordered(l):
    for i in range(len(l)-1):
        if l[i+1] < l[i]:
            return False
    return True

l = generate_random_list(25, -100, 100)
print("UNSORTED: ", l)
quicksort(l)
print("SORTED:   ", l)
if check_ordered(l):
    print("Verified")
else:
    print("Algorithm failed")
