
# tant que a est sup ou égale à zéro, la boucle boucle
a = 15

while a >= 0:
    print(a)
    a -= 1

print('fin')
print('#' * 15)


# Une liste de 15 zéro est créé, tant que la liste n'est pas à 0, la boucle boucle
a = [0] * 15
print(a)

while a:
    objet = a.pop()
    print(objet)

print(a)
print('fin')
print('#' * 15)

# Comment arrêter une boucle avec break

a = 10
while a:
    a += 2
    if a % 1000 == 0:
        break
    print(a)

print(a)
print('fin')
print('#' * 15)