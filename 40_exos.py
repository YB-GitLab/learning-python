# Les structures if-elif-else

# Exercice 1 - Une erreur courante à connaître absolument

prix = 10
monnaie = 10

#if prix = monnaie:
if prix == monnaie:
    print('pile le montant')


# Exercice 2 - Une autre erreur et utilisation du modulo dans les IF
valeur = -4
if valeur % 2 == 0:
    print(f'{valeur} est pair')
else:
    print(f'{valeur} est impair')

# Exercice 3 - Structures conditionnelles et listes
liste = []

if liste:
    print('Liste non vide', liste)
elif not liste:
    print('liste vide')

# Exercice 4 - Chaînes de caractères et IF

texte = ""
if texte:
    print('texte non vide', texte, '!')
elif not texte:
    print('texte vide', texte, '!')

# Exercice 5 - Contourner les KeyError avec les dictionnaires et les if

menu = {"Poisson":100,
        "Végé":56,
        "Pizza":80,
        "Salade":27}
choix = "Citrouille"
prix = menu.get(choix, -1)

print("Pour debug :", choix, prix, '\n')

if prix != -1:
    print(f'Vous avez choisi {choix}, le coût est de {prix} euros')
else:
    print(f'le plat {choix}, n\'est pas dispo')

# Exercice 6 - Bien utilisez les if-elif-else.

age = 35
if age <= 0:
    print('pas encore né')
elif age < 6:
    print('moins de 5 ans')
elif age < 18:
    print('ado')
elif age < 30:
    print('jeune adulte')
elif age < 100:
    print('adulte')
elif age < 200:
    print('centenaire')
else:
    print('le secret de la longevité, 5 fruits et légumes par jour')

# Exercice 7 - Attention à la casse, utilisez la méthode lower() pour comparer des strings entre elles.

message_utilisateur = 'AIDE'
#message_utilisateur = message_utilisateur.lower()
if message_utilisateur.lower() == 'aide':
    print('afficher l\'aide')
else:
    print('On passe à la suite')

# Exercice 8 - Se repérer dans une structure if-elif-else complexe

note_au_bac = -10
print('Pour débug', note_au_bac, '\n')
message = ''
if note_au_bac >= 10:
    message += 'Félicitations, Vous avez le Bac '
    if 12 <= note_au_bac <= 14:
        message += 'avec la mention "Assez-bien'
    elif 14 <= note_au_bac <= 16:
        message += 'avec la mention "bien"'
    elif 16 <= note_au_bac:
        message += 'avec mention "Très bien"'
    else:
        message += 'sans mention'
else:
    if note_au_bac > 8:
        message += 'rattrapage'
    else:
        message += 'désolé'
print(message)
