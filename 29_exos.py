# Split & Join
# Exercice 1 - Split sans rien entre les parenthèses
jours = 'lundi mardi mercredi jeudi vendredi samedi dimanche'
liste_jours = jours.split()
print(jours)
print(liste_jours) # ['lundi', 'mardi', 'mercredi', …
jours = '-'.join(liste_jours)
print(jours) # lundi-mardi-mercre …
print('--------')
# Exercice 2 - Comprendre l'utilisation de split & join
# Le but de ce code est d'afficher les jours séparés par des espaces
jours = 'lundi-mardi-mercredi-jeudi-vendredi-samedi-dimanche'
liste_jours = jours.split('-')
print(jours)
print(liste_jours)
jours = ' '.join(liste_jours)
print(jours)
print('--------')
# Exercice 3 - Découverte des fonctionnalités avancées des listes
nom_fichier1 = 'pas_musique.mp3.exe'
nom_fichier2 = 'musique.mp3'
fichier1_extension = nom_fichier1.split('.')[-1]
fichier2_extension = nom_fichier2.split('.')[-1]
print('Fichier 1 est un mp3?', fichier1_extension == 'mp3') # False
print('Fichier 3 est un mo3?', fichier2_extension == 'mp3') # True
