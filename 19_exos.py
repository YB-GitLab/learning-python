# len() et str()

# Exercice 1 - Len d'un caractère?

caractère_vide = "" # 0
caractère_classique = "A" # 1
caractère_spécial = "\n" # 1

print(len(caractère_vide))
print(len(caractère_classique))
print(len(caractère_spécial))

print('--------')

# Exercice 2 - Savoir compter les caractères est HYPER-IMPORTANT

#casse_tête = '\ \ \ \ \' # -->_erreur
casse_tête = '\ \ \ \ \ \\' # 11
print(len(casse_tête))

# Exercice 3 - Alors entraînons nous...

casse_tête = '\n \n \n \n \n \n' # 11
print(len(casse_tête))

# Exercice 4 - Encore.

alphabet = 'abcdefg\n\nhijklmn\nopqrstuvwxyz\n'# 30
print(len(alphabet))

# Exercice 5 - ... Encore.

citation = "Andréa a dit: \n1, 2, 3, Soleil\n." # 32
print(len(citation))

# Exercice 6 ⭐Et encore.

# Je suppose que 4 des """ """ sont comptés
multiligne = """Des caractères supplémentaires se cachent
dans les strings multilignes...
saurez-vous les démasquez???
\n
"""
n_caractères_multiligne = len(multiligne)
ligne_1 = "Des caractères supplémentaires se cachent" # 41
ligne_2 = "dans les strings multilignes..." # 31
ligne_3 = "saurez-vous les démasquez???" # 28
ligne_4 = "\n" # 1
n_caractères_ligne_par_ligne = len(ligne_1) + len(ligne_2) + len(ligne_3) + len(ligne_4)
différence = n_caractères_multiligne - n_caractères_ligne_par_ligne
print(différence)

# Exercice 7 - Concaténation String & Types Numériques

message = "Andréa à dit: \"" + str(1) + ", " + str(2) + ", " + str(3) + ", Soleil\"."

print(message)