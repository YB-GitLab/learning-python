# Glouton (greedy) | Brute force
# Rendre la monnaie

# 121
# 50, 20, 10, 5, 2, 1
# 2 x 50
# 1 x 20
# 1 x 1

# 121
# 50, 20, 10, 5, 2
# 2 x 50
# 1 x 20
# ...  # Dans le fonctionnement de l'algorithme glouton, il ne rendra pas la monnaie (car il ne peut pas rendre de pièce de 1)

# 2 x 50
# 1 x 10
# 1 x 5
# 3 x 2  # Avec le Brute force (qui aura testé toutes les combinaisons en commençant par 2), il porra rendre la monnaie

# s = somme à rendre
# e = le tableau des monnaies disponibles


def cash_back_greedy(s, e):

    r = []
    for a in e:
        n = s//a
        r.append(n)
        s -= n*a

    for i in range(len(e)):
        if r[i] > 0:
            print(e[i], "x", r[i])

    if s > 0:
        print("Reste de :", s)
    else:
        print("Toute la monnaie a été rendue")

    return r

def cash_back_brute_force(s, e):

    results = []
    c = [0]*len(e)  # on commence par créer une liste contenant que des zéros fois la longueur des types de monnaies disponibles
    while(c[0]*e[0] <= s):

        '''v = 0
        for i in range(len(e)):
            v += c[i]*e[i]'''
        v = sum([c[i]*e[i] for i in range(len(e))])
        if v == s:
            results.append((c.copy(), sum(c)))
            #print("Result found : ", c)

        c[-1] += 1

        i = len(e)-1
        while(v > s):
            c[i] = 0
            c[i-1] += 1
            v = sum([c[i]*e[i] for i in range(len(e))])
            i -= 1
            if i == 0:
                break
    print("END")
    results.sort(key=sort_results, reverse=True)
    for r in results:
        print(r)

def sort_results(e):
    return e[1]


#print(cash_back_greedy(121, [50, 20, 10, 5, 2]))
cash_back_brute_force(121, [50, 20, 10, 5, 2])