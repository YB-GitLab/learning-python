# Travailler avec les boucles WHILE

# Exercice 1 - Commençons facile

nombre = 10
while nombre >= 0:
    print(nombre)
    nombre = nombre - 1
# cela va décrementer nombre de 10 à 0

print('#' * 15)
# Exercice 2 - Sortir d'une boucle infinie

nombre = 0
while nombre > 0:
    print(nombre)
    nombre += 1
print('#' * 15)
# Exercice 3 - Comprendre l'instruction break

nombre = 10
while True:
    print(nombre)
    nombre = nombre - 1
    if nombre < 0:
        break
# ca va décrémenter la var nombre jusque -1 puis break

# Exercice 4 - Affichez tous les nombres de 10 à 19

nombre = 10
while nombre <= 19:
    print(nombre)
    nombre += 1
print('#' * 15)
# Exercice 5 - Affichez tous les multiples de 3 contenus entre 10 et 100

nombre = 10
while nombre <= 100:
    if nombre % 3 == 0:
        print(nombre)
    nombre += 1
print('#' * 15)
# Exercice 6 - Créez une liste des multiples de 3 contenus entre 10 et 100

nombre = 10
liste = []
while nombre <= 100:
    if nombre % 3 == 0:
        liste.append(nombre)
    nombre += 1
print(liste)

print('#' * 15)
# Exercice 7 - IndexError et boucles While

liste = ['A', 'B', 'C', 'D', 'E', 'F', 'G']

index = 0
while index <= 0:
    while index < len(liste):
        print(liste[index])
        index = index + 1

print('#' * 15)
# Exercice 8 - Indexing Négatif et boucle While

liste = ['A', 'B', 'C', 'D', 'E', 'F', 'G']

index = -1
while index >= - len(liste):
    print(liste[index])
    index = index - 1