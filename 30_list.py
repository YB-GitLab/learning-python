# Les listes

# Deux listes comportants les mêmes objets mais dans un ordre différent ne sont pas identiques
liste_A = [1, "20", "BAH"]
liste_B = ["20", 1, "BAH"]

print(liste_A == liste_B) # False
print('--------')

# une liste peut-être vide ou avec autant d'éléments que l'on veut

liste_A = []
liste_B = [True] * 15

print(liste_A)
print(liste_B)
print('--------')

# Une liste peut contenir tout les types : des booléens, des nombres (int / float), des fonctinos, des listes, des string

liste_A = [True, 100, print, ['a', 'B'], 'Hello']

print(liste_A)
print('--------')

# Elle est adaptable car on peut rajouter des éléments à la liste à la volée

liste_A = [True, 100]
print(liste_A)

liste_A.append("New")
print(liste_A)


print('-------')

# On peut supprimer des choses à la liste

liste_A = [True, 100]
print(liste_A)

liste_A.pop()
print(liste_A)
print('----------')
# on peut utiliser del pour supprimer un objet de la liste

liste_A = [True, 100]
print(liste_A)

del liste_A[0]
print(liste_A)
print('----------')
# on peut modifier un élément particulier à la liste

liste_A = [True, 100, False]
print(liste_A)

liste_A[1] = 'New'
print(liste_A)
print('---------')
# Comme vu plus haut, on peut rajouter des éléments avec append
# Append va rajouter à droite de la liste
liste_A = [True, 100]
liste_A.append(20)
liste_A.append(30)
liste_A.append(['Hi', 'I am'])
print(liste_A)
print('----------')

# On peut utiliser remove() pour supprimer un élément souhaité
liste_A = ['objet1', 100]
liste_A.append(20)
liste_A.remove('objet1')
print(liste_A)
print('-------')
# On peut utiliser pop() pour supprimer un élément
liste_A = ['elem0','elem1', 'elem2']

liste_A.pop() # supprime le dernier élément
liste_A.pop(0) # supprime le premier élément en utilisant l'index
liste_A.pop(-1) # supprime le dernier élément en utilisant l'index
print(liste_A)
print('--------')

# les opérateurs sur les listes + * in

# l'opérateur + concaténation : les 2 opérandes doivent être des listes
liste_A = ['hi', 'Hello']
print(liste_A + ['bye', 'bye'])
# print(liste_A + 10) -> erreur
# print(liste_A + True) -> erreur pareil pour str, les fonctions
print('-------')
# l'opérateur * répétition
liste_A = ['Hi', 'Hello']
print(liste_A * 5) # répète 5 fois
print(liste_A * 0) # vide la liste (comme pour les négatifs)
print('-----')
# l'opérateur in recherche
liste_A = ['Hi', 'Hello']
print('Hi' in liste_A)
print('hi' in liste_A)
# l'opérateur +=
liste_A = ['Hi', 'Hello']
liste_A += [30, 40, 50]
print(liste_A)
liste_A += "Yop"
print(liste_A)
print('--------')

# les listes sont indexables

liste_A = ['Hi', 'Hello', 'Bonjour']
liste_A[0] = "Hi, Hi"
print(liste_A)
print('-----------')
# Faire attention de ne pas sortir de la liste ex avec len()
liste_A = ['Hi', 'Hello', 'Bonjour']
index_max = len(liste_A) - 1
print(liste_A[index_max])
# index_max = len(liste_A) # --> erreur "index out of range"
# peut-on ajouter un objet
liste_A = ['Hi', 'Hello', 'Bonjour']
index_max = len(liste_A) - 1
# liste_A[index_max + 1] = "New" # --> erreur "index out of range"
print('--------')
# On peut utiliser les slices pour modifier / rajouter des objets
liste_A = ['Hi', 'Hello', 'Bonjour']
liste_A[1:2] = ['Coucou', 'Yo']
print(liste_A)
# on peut les rajouter à la suite
liste_A = ['Hi', 'Hello', 'Bonjour']
liste_A[3:4] = ['Coucou', 'Yo']
print(liste_A)
print('----------')
# on peut faire de la recherche de numéro d'index
liste = [1, 2, 'Yo']
print(liste.index('Yo'))
print('-------')
# on peut recherché des éléments dans une liste imbriquée
liste = [1, 2, ['A', 'B']]
print(liste[2][0])
# et dans une imbriqué dans l'imbriqué
liste = [1, 2, ['A', 'B', ['Z', 'Y']]]
print(liste[2][2][1])

print('#' * 21)
# Bonnes pratiques pour les listes

symboles = ['+', '-', '*', '/']
operations = ['addition', 'soustraction',
              'murtiplication', 'division']

template = 'Le symbole de l\'opération {op} est {sym}'
print(template.format(op=operations[0], sym=symboles[0]))
print(template.format(op=operations[1], sym=symboles[1]))
print(template.format(op=operations[2], sym=symboles[2]))
print(template.format(op=operations[3], sym=symboles[3]))

print('#' * 21)


