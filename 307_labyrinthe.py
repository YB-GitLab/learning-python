# Labyrinthe (Maze)
#  Tableaux à 2 dimensions
#  Récursivité
#  Recherche de chemin

#créer un labyrinthe
# 0 : les murs
# 1 : chemin
# 8 : entrée
# 9 : sortie

# x = 0  1  2  3  4  5  6  7      y
m = [[8, 1, 1, 1, 1, 1, 1, 1],  # 0
     [0, 0, 1, 0, 0, 1, 0, 0],  # 1
     [1, 1, 1, 0, 0, 1, 0, 1],  # 2
     [0, 0, 0, 1, 1, 1, 0, 1],  # 3
     [0, 1, 1, 1, 0, 0, 0, 1],  # 4
     [0, 1, 0, 1, 1, 1, 1, 1],  # 5
     [0, 1, 0, 0, 0, 1, 0, 1],  # 6
     [0, 1, 1, 0, 0, 1, 0, 9],] # 7


def find_maze_exit(m, x, y):
     if m[y][x] == 9:
          print("Maze exit found at:", x, y)
          for l in m:
               d = ""
               for e in l:
                    if e == -1:
                         d += str(e) + " "
                    else:
                         d += " " + str(e) + " "
               print(d)

          exit(0)

     m[y][x] = -1  # les cases explorées deviennent -1

     # on explore à gauche
     if x > 0:
          if m[y][x-1] >= 1:
               find_maze_exit(m, x-1, y)

     # on explore à droite
     if x < len(m[0])-1:
          if m[y][x+1] >= 1:
               find_maze_exit(m, x+1, y)

     # on explore en haut
     if y > 0:
          if m[y-1][x] >= 1:
               find_maze_exit(m, x, y-1)

     # on explore en bas
     if y < len(m)-1:
          if m[y+1][x] >= 1:
               find_maze_exit(m, x, y+1)





find_maze_exit(m, 0, 0)