# Données monotones (monotonic values)
# increasing (soit les données s'incrémentent...
# decreasing (... soit elles se décrémentent)

a = [1, 5, 6, 9, 11, 11, 12, 15]  # données monotones
b = [1, 5, 6, 6, 5, 7, 10]  # données non monotones

def is_increasing_monotonic_values_1(l):
    for i in range(len(l)-1):
        if l[i+1] < l[i]:
            return False
    return True

def is_increasing_monotonic_values_2(l):
    return l == sorted(l)  # version courte de ce qui suit (on peut rajouter reversed = True pour trier dans l'autre sens
    # if l == sorted(l):
    #     return True
    # return False


print("a:", is_increasing_monotonic_values_2(a))
print("b:", is_increasing_monotonic_values_2(b))