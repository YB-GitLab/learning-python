### EXERCICES ###

print('---------------------------')
# Exercice 1 - Déclaration d'objets de type int et opérations mathématiques

# 1- Quelles sont les fonctions que vous utilisez dans ce bout de code ? À quoi servent-elles?
# déclaration de variable, print (pour afficher dans le stdout), type renvoie le type (int, float …)
# 2- Pour chaque opération mathématique de base, quelle est le type du résultat ?
# Elles reverront toutes des int sauf le quotient qui renverra un float

a = 5
b = 10

somme = a + b
produit = a * b
différence = a - b
quotient = a / b

print(a, type(a))
print(b, type(b))
print(somme, type(somme))
print(produit, type(produit))
print(différence, type(différence))
print(quotient, type(quotient))

print('---------------------------')
# Exercice 2 - Déclaration d'objets de type int et opérations mathématiques
# 1- Lorsque j'écris du code, comment puis je reconnaître un float d'un int?
# Les int sont des entiers, les floats sont de nombres à virgule frottante qui contient donc un point
# 2- Lorsque je déclare un float sans indiquer de chiffre(s) à gauche du point, que se passe-t-il?
# Un 0 sera rajouté
# 3- Lorsque je déclare un float sans indiquer de chiffre(s) à droite du point, que se passe-t-il?
# Un 0 sera rajouté
# 4- Lorsque je déclare un float avec seulement des zéros "après la virgule", est ce toujours un float?
# Oui

a = .005
b = 1.
c = 12.10
d = 12.0

print(a, type(a))
print(b, type(b))
print(c, type(c))
print(d, type(d))

print('---------------------------')
# Exercice 3 - Opérations mathématiques mélangeant float et int
# 1- Lorsque je fais une opération mathématique mélangeant des int et des float, le résultat est il un int ou bien un float?
# Même si le résultat tombe "juste" (c.à.d. avec que des zéros après la virgule)
# Ce sera tjs un float

n_entier = 10
n_virgule = 5.0

somme = n_entier + n_virgule
produit = n_entier * n_virgule
différence = n_entier - n_virgule
quotient = n_entier / n_virgule

print(somme, type(somme))
print(produit, type(produit))
print(différence, type(différence))
print(quotient, type(quotient))

print('---------------------------')
# Exercice 4 - Conversion d'un objet float et un objet de type int
# 1- Que se passe-t-il quand je converti un float en int avec la fonction int()?
# Ça tronquera en sortie
# 2- Que se passe-t-il quand je converti un float en int avec la fonction round()?
# Ça arrondira à l'inférieur ou au supérieur
# 3- Quelle est la différence entre la fonction int() et la fonction round()?

# Script 4.1 - conversion avec la fonction int()

a = 4.9
b = 4.51
c = 4.5
d = 4.49
e = 4.1
print('conversion avec la fonction int()')
print(a, int(a))
print(b, int(b))
print(c, int(c))
print(d, int(d))
print(e, int(e))

# Script 4.2 - conversion à l'aide la fonction round()

a = 4.9
b = 4.51
c = 4.5
d = 4.49
e = 4.1
print ('conversion à l\'aide la fonction round()')
print(a, round(a))
print(b, round(b))
print(c, round(c))
print(d, round(d))
print(e, round(e))

print('---------------------------')
# Exercice 5 - Déclarations de chaines de caractères str
# Est ce qu'il y a une différence entre déclarer mes str avec des guillemets simples et des guillemets doubles?
# non

# Script 5.1 - Déclaration de chaînes de caractères mono-lignes
single_quotes = 'Hello World'
double_quotes = "Hello World"

print(single_quotes, type(single_quotes))
print(double_quotes, type(double_quotes))

#Script 5.2 - Déclaration de chaînes de caractères multi-lignes

triple_singles_quotes = '''
Hello World

Bye World'''
print(triple_singles_quotes, type(triple_singles_quotes))

triple_doubles_quotes = """
Hello world

Bye World"""
print(triple_doubles_quotes, type(triple_doubles_quotes))

print('---------------------------')
#Exercice 6 - Séquence échappement & Backslash

# 1- Comment puis-je faire pour sauter une ligne à l'intérieur d'une str mono-ligne?
# En utilisant le \n
# 2- Si je déclare un str avec des guillemets simples, comment puis-je y intégrer du texte qui contient un guillemet simple (ou apostrophe)?
# oui mais il faudra rajouter le \_d'échappement
# 3- Si je déclare un str avec des guillemets doubles, comment puis-je y intégrer du texte qui contient une paire de guillemet double?
# pareil en rajoutant le \ d'échappement
# 4- Récrivez le code sans ajouter les backslashs et étudiez les erreurs qui en résultent.

texte_multiligne = "Une ligne\nDeux lignes"
print(texte_multiligne)
texte_échappé_1 = "Et là je mets des \"double quotes\" dans ma string"
print(texte_échappé_1)
texte_échappé_2 = 'Il n\'y a que des singles quotes dans c\'te ligne de code, n\'est ce pas?'
print(texte_échappé_2)

print('---------------------------')
# Exercice 7 - Raw Strings
# Que se passe-t-il si j'enlève le 'r' juste avant d'ouvrir les guillemets pour déclarer ma str.
# Ça prend en condidération tous les \ d'échappement

texte_échappé_raw = r'C:\windows\users\dossier'
print(texte_échappé_raw)

print('---------------------------')
# Exercice 8 - Déclaration de list
# 1- Est il possible de stocker des objets de type différents dans une même liste?
# Oui
# 2- Quelles sont les deux manières de déclarer une liste?
# list() et []
# 3- Est-ce que ces deux manières sont équivalentes?
# oui

une_liste = list()
une_autre_liste = []
encore_une_liste = [0, 1, 2, 3, "soleil"]

print(une_liste, type(une_liste))
print(une_autre_liste, type(une_autre_liste))
print(encore_une_liste, type(encore_une_liste))

print('---------------------------')
# Exercice 9 - Déclaration de booléen
# Recopiez le code pour vous familiariser avec la déclaration d'objets booléens.

vrai = True
faux = False

print(vrai, type(vrai))
print(faux, type(faux))

print('---------------------------')
# Exercice 10 - True et False sont des 1 et 0 déguisés
# 1- Grace aux résultats donnés par l’exécution du script ci-dessous, pouvez vous conclure que True a pour valeur numérique 1?
# oui
# 2- Que False a pour valeur numérique 0?
# oui

print(True + True + True)
print(False + False + False)
print(True + True + False)
print(False + False + True)

print('---------------------------')
# Exercice 11 - Évaluation booléenne de différents objets

# 1- Dans quelle condition un objet de type list est il évalué à True? à False?
# 2- Dans quelle condition un objet de type str est il évalué à True? à False?
# 3- Dans quelle condition un objet de type numérique (float ou int, peu importe) est il évalué à True? à False?
# Dans les trois cas, c'est lorsque la variable est vide ou égale à 0 pour false, pour tout le reste ce sera true

liste_vide = []
string_vide = ''
liste_non_vide = [1, 2]
string_non_vide = 'hey'
zéro = 0
pas_zéro = -0.01

print(liste_vide, bool(liste_vide))
print(string_vide, bool(string_vide))
print(liste_non_vide, bool(liste_non_vide))
print(string_non_vide, bool(string_non_vide))
print(zéro, bool(zéro))
print(pas_zéro, bool(pas_zéro))
