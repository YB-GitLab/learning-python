## Pratiquer sur les notions déjà vues
# - Boucles for/while
# - Variables numériques / booléennes
# - Nombre aléatoire
# - Break

import random


def demander_nombre(nb_min, nb_max):
    nombre_int = 0
    while nombre_int == 0:
        nombre_utilisateur = input(f"Quel est votre nombre ? (entre {nb_min} et {nb_max}) : ")
        # if not int(nombre_utilisateur.isdigit()):
        #     print('Votre nombre doit être un nombre')
        #     nombre_int = 0
        # elif nb_min <= int(nombre_utilisateur) <= nb_max:
        #     nombre_int = int(nombre_utilisateur)
        # else:
        #     print(f'Votre nombre doit-être entre {nb_min} et {nb_max}')
        #     nombre_int = 0
        try:
            nombre_int = int(nombre_utilisateur)
        except:
            print(("ERREUR, Entrez un nombre"))
        else:
            if not nb_min <= nombre_int <= nb_max:
                print(f"ERREUR, Entrez un nombre entre {nb_min} et {nb_max}")
                nombre_int = 0

    return nombre_int



def trouver_le_nombre(nombre_magique):
    """
    nombre = 0
    coups = nombre_de_coup
    while not nombre == nombre_magique and coups > 0:
        print(f"Il vous reste {coups} coups")
        nombre = demander_nombre(nombre_min, nombre_max)
        trop_grand = nombre > nombre_magique
        # trop_petit = nombre < nombre_magique
        egalite = nombre == nombre_magique
        #print(nombre, type(nombre))
        if egalite:
            print('Vous avez trouvé')
        elif trop_grand:
            print('Plus grand')
            coups -= 1
        else:
            print('Trop petit')
            coups -= 1
    if coups == 0:
        print("Vous avez perdu !!! Le nombre mystère était : " + str(nombre_magique))
    """
    gagne = False
    for i in range(0, nombre_de_coup):
        coups = nombre_de_coup - i
        print(f"Il vous reste {coups} coups")
        nombre = demander_nombre(nombre_min, nombre_max)
        trop_grand = nombre > nombre_magique
        # trop_petit = nombre < nombre_magique
        egalite = nombre == nombre_magique
        #print(nombre, type(nombre))
        if egalite:
            print('Vous avez trouvé')
            gagne = True
            break
        elif trop_grand:
            print('Plus grand')
        else:
            print('Trop petit')
    if not gagne:
        print("Vous avez perdu !!! Le nombre mystère était : " + str(nombre_magique))

nombre_min = 1
nombre_max = 10
nombre_de_coup = 4
nombre_magique = random.randint(nombre_min, nombre_max)

trouver_le_nombre(nombre_magique)
# print(nombre)
