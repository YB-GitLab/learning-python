# Serialization, Pickle

import pickle

dico = {"Hey": 'you',
        "comment": 'vas-tu'}
liste = ['1', 2, 3]
#pickle les objets
with open('dico.pkl', 'wb') as f:  # attention icic c'est 'wb'
    pickle.dump(dico, f)  # attention à l'écriture ici .dump en prem's, puis le (objet , f) <-- f pour fichier
with open('liste.pkl', 'wb') as f:
    pickle.dump(liste, f)

# Deserializer
import pickle

# deserialization
with open('dico.pkl', 'rb') as f:  # <-- ici c'est 'rb'
    dico = pickle.load(f)  # attention à al syntaxe
with open('liste.pkl', 'rb') as f:
    liste = pickle.load(f)
