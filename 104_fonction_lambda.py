##  Fonction Lambda


# def mult_callback(a, b):
#     return a*b
#
#
# def add_callback(a, b):
#     return a+b


def afficher_table(n, operateur_str, operation_cbk):  # on rajoute une variable_cbk
    for i in range(1, 10):
        print(i, operateur_str, n, "=", operation_cbk(i, n))  # on appelle ici la variable avec les (a, b)


# fonctions lambda
afficher_table(2, "x", lambda a, b: a * b)  # On appelle la fonction "lambda a, b : 'ici' le coeur de la fonction"
print()
afficher_table(2, "/", lambda a, b: a / b)
print()
afficher_table(2, "+", lambda a, b: a + b)
print()
afficher_table(2, "-", lambda a, b: a - b)
