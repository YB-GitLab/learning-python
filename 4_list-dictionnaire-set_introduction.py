# décalration des listes grâce au builtin list() et les []

obj1 = list()
obj2 = []

print(obj1, type(obj1))
print(obj2, type(obj2))

print('-------------------------')
# déclaration d'une liste populée

ma_Liste = [1, 2, 3, 'soleil']
print(ma_Liste, type(ma_Liste))

print('--------------------------')
# décalration de dictionnaire vide et de set vide

mon_dict = {}
mon_dict2 = dict()
mon_set = set()

print(mon_dict, type(mon_dict))
print(mon_dict2, type(mon_dict2))
print(mon_set, type(mon_set))