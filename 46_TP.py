# récupérer les arguments
import sys

_, *liste = sys.argv

# si pas de liste en arg :
if not liste:
    liste = ['4', '7', '1.1']
    print("Debug")
    print(liste)

# Convertir les str en float
for i in range(len(liste)):
    liste[i] = float(liste[i])

# rechercher le minimum de la liste de float
minimum = liste[0] # initialise notre variable de recherche au premier élement de la liste
for nombre in liste: # parcourir chaque nombre de la liste
    print("\n\nVariable de recherche:", minimum, '\nNonbre actuel', nombre)
    if nombre < minimum: # si on découvre un nombre plus petit que celui de notre variable de recherche
        minimum = nombre # . . . alors on met à jour notre variable de recherche
        print("MISTE À JOUR, nouvelle valeur de recherche:", minimum)

# affiche notre résultat ainsi que affiche la solution trouvée par le builtin min()
print('pour la liste\n', liste)
print('Nous avons trouvé le minimum :', minimum)
print('Le buitin min() a trouvé     :', min(liste))

