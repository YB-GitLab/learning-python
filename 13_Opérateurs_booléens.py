# les opérateurs booléens
# python résoud d'abord les Not puis les And puis les or ( les paranthèses en premier

print(not True)

print(True and False)

print(True or False)

print('------------')

# un peu plus poussé

expr1 = not True or True
expr2 = not (True or True)

print(expr1)
print(expr2)

print('----------')
# Not a bien la priorité sur la porte Or

expr1 = not True or True
print()
expr2 = not (True or True)
expr3 = (not True) or True

print(expr1)
print(expr2)
print(expr3)

print('----------')
#not a bien la priorité sur la porte And
expr1 = not False and False
expr2 = (not False) and False
expr3 = not (False and False)

print(expr1)
print()
print(expr2)
print(expr3)

print('-----------')
# And a bien la priorité sur la porte Or

expr1 = True and True or False and False
expr2 = (True and True) or (False and False)
expr3 = True and (True or False) and False

print(expr1)
print()
print(expr2)
print(expr3)
