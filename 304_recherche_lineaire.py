

l = [1, 6, 3, 7, 9, 4, 6, 7, 10, 2, 1, 7]

def linear_search(l, e):
    for i in range(len(l)):
        if l[i] == e:
            return i
    return -1

print(linear_search(l, 11))

