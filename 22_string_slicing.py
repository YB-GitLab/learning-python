# Le string slicing

texte = 'Hello World'

print(texte[::])
print(texte[:])
print(texte[6:])
print(texte[0:5])
print(texte[::2])
print(texte[0:11:1])
print(texte[3:5])
print(texte[10000 ::])
print(texte[-5 ::])

print(texte[::-1])
