# La contatennation de <str>

texte1 = 'Hello'
texte2 = 'World'

print(texte1 + ' ' + texte2 + '\n' +'\n')
print("H" + 'e' + 'l' + 'l' + 'o')

print('---------')
# La multiplication

print('H' * 7)

print('----------')
# IN
nom_fichier = 'audio.mp3'
nom_fichier2 = 'video.mp4'

print('.mp4' in nom_fichier, '.mp4' in nom_fichier2)