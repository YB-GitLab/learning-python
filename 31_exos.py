# S'exercer avec les listes

# Exercice 1 - Positive & Negative Indexing
liste = ['aa', 'bb', 'Fizz', 'dd']
pos_index = 0
print(f'{pos_index} ->', liste[pos_index]) # 0 -> aa
neg_index = -1
print(f'{neg_index} ->', liste[neg_index]) # -1 -> dd

# Ensuite, trouver les index positifs et négatifs qui permettent d'atteindre la valeur "Fizz" dans la liste.
pos_index = 2
print(f'{pos_index} ->', liste[pos_index]) # doit afficher Fizz
neg_index = -2
print(f'{neg_index} ->', liste[neg_index]) # doit afficher Fizz

print('-----')

# Exercice 2 - Modifier une liste (indexing positif)

liste = ['aa', 'bb', 'Fizz', 'dd']
pos_index = 0
liste[pos_index] = 'oo'
print(liste) # va afficher ['oo', 'bb', 'Fizz', 'dd']
# Ensuite, trouver l'index positif qui permet de remplacer la valeur 'Fizz' par la valeur 'oo' dans la liste.
liste = ['aa', 'bb', 'Fizz', 'dd']
pos_index = 2
liste[pos_index] = 'oo'
print(liste)
print('-------')

# Exercice 3 - Modifier une liste (indexing negatif)

liste = ['aa', 'bb', 'Fizz', 'dd']
neg_index = - len(liste)
liste[neg_index] = 'oo'
print(liste) # ['oo', 'bb', 'Fizz', 'dd']
# Ensuite, trouver l'index négatif qui permet de remplacer la valeur 'Fizz' par la valeur 'oo' dans la liste
liste = ['aa', 'bb', 'Fizz', 'dd']
neg_index = - len(liste) + 2
liste[neg_index] = 'oo'
print(liste)
print('----')

# Exercice 4 - Index Maximal Positif
# Recopier le code ci-dessous manuellement, puis remplacer les points d'interrogations par l'index positif du dernier élément de la liste
liste = ['aa', 'bb', 'cc']
max_pos_index = len(liste) - 1
print(liste[max_pos_index])
print('--------')

# Exercice 5 - Index Maximal Négatif
liste = ['aa', 'bb', 'cc']
max_neg_index = - len(liste)
print(liste[max_neg_index])
print('--------')

# Exercice 6 - Pop and Remove

liste = ['Premier', 'a', 'b', 'Fizz', 'c', 'Dernier']
premier = liste.pop(0)
dernier = liste.pop()
print(premier)
print(dernier)
liste.remove('Fizz')
print(liste)
print('------')

# Exercice 7 - Créer une liste qui contient l'élément zéro dix fois.

dix_zéros = [0] * 10
print(dix_zéros)
print('--------')

# Exercice 8 - Ajouter des éléments à une liste avec la méthode append()

liste = [1, 2, 3, 4, 5]
liste.append(6)
liste.append(7)
liste.append(8)
print(liste)
print('---------')

# Exercice 9 - Ajouter des éléments à une liste avec l'opérateur + (plus)

liste = [1, 2, 3, 4, 5]
liste += [6, 7, 8]
print(liste)
print('--------')

# Exercice 10 - Supprimer avec la méthode remove()

liste = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

liste.remove(1)
liste.remove(3)
liste.remove(5)
liste.remove(7)
liste.remove(9)

print(liste)

# Exercice 11 ⭐Mp3 ou pas Mp3? Tel est la question.

nom_fichier1 = 'pas_musique.mp3.exe'
nom_fichier2 = 'musique.mp3'

bouts_de_nom_fichier1 = nom_fichier1.split('.')
bouts_de_nom_fichier2 = nom_fichier2.split('.')

fichier1_extension = bouts_de_nom_fichier1[-1]
fichier2_extension = bouts_de_nom_fichier2[-1]

print('Fichier 1 mp3 ?', fichier1_extension.lower() == 'mp3')
print('Fichier 2 mp3 ?', fichier2_extension.lower() == 'mp3')

