# Index des strings

# Exercice 1 - Accéder au caractère 'a'

alphabet = 'abcdefghijklmnopqrstuvwxyz'

pos_index = 0
neg_index = -26

print(alphabet[pos_index])
print(alphabet[neg_index])

print('------------')

# Exercice 2 - Accéder au caractère 'z'

alphabet = 'abcdefghijklmnopqrstuvwxyz'

pos_index = 25
neg_index = -1

print(alphabet[pos_index])
print(alphabet[neg_index])

print('-------')

# Exercice 3 - Accéder au caractère 'j'

alphabet = 'abcdefghijklmnopkrstuvwxyz'

pos_index = 9
neg_index = -17

print(alphabet[pos_index])
print(alphabet[neg_index])

print('-------')

# Exercice 4 - Accéder au caractère 'u'

alphabet = 'abcdefghijklmnopkrstuvwxyz'

pos_index = 20
neg_index = -6

print(alphabet[pos_index])
print(alphabet[neg_index])

print('---------')

# Exercice 5 - Accéder au caractère 'm'

alphabet = 'abcdefghijklmnopkrstuvwxyz'

pos_index = 12
neg_index = -14

print(alphabet[pos_index])
print(alphabet[neg_index])

print('------')

# Exercice 6⭐Accéder au caractère du milieu

texte = 'salut'

index_milieu = round(len(texte) / 2) + 1
# index_milieu = (- int(- len(texte) // 2))

print(index_milieu)
print(type(index_milieu))

print(texte[- index_milieu])

print('---------')

alphabet = 'abcdefghijklmnopkrstuvwxyz'

pos_index = len(alphabet) -1
neg_index = - len(alphabet)

print(alphabet[pos_index])
print(alphabet[neg_index])



