## Pizzas V2
# - POO
#     - Faire vos premiers pas
#     - Mettre en pratique des concept
#     - Faire évoluer le projet Pizzas
#     - Héritage
# - Notions Précédentes :
#     - Collections
#     - Conditions
#     - Fonctions

# nom, prix, ingrédients, végétarienne

class Pizza:
    def __init__(self, nom, prix, ingredients, vegetarienne=False):  # Le constructeur
        self.nom = nom
        self.prix= prix
        self.ingredients = ingredients
        self.vegetarienne = vegetarienne
    def Afficher(self):
        veg_str = ""
        if self.vegetarienne:
            veg_str = " - VEGETARIENNE"
        template = (f"PIZZA {self.nom} : {self.prix} €" + veg_str)
        print(template)
        # print(self.ingredients)
        print(", ".join(self.ingredients))
        print()


## pizzapersonnalisee  [[héritage]]

class PizzaPersonnalisee(Pizza):  # Nouvelle classe qui hérite de la classe Pizza (entre parenthèse)
    PRIX_DE_BASE = 7
    PRIX_INGREDIENT_SUPPLEMENTAIRE = 1.2
    dernier_numero = 0
    def __init__(self):  # créé le contructeur (soit son propre init)
        PizzaPersonnalisee.dernier_numero += 1
        self.numero = PizzaPersonnalisee.dernier_numero
        super().__init__("Personnalisée" + str(self.numero), 0, [])  # appelle le init du parent (Pizza)
        self.demander_ingredients_utilisateur()
        self.calculer_le_prix()

    def demander_ingredients_utilisateur(self):  # ajout d'une fonction dans cette classe
        print()
        print(f"Ingrédients pour la pizza personnalisée {self.numero}")
        while True:  # while true = tu boucles toujours (ici tu boucles tjs tant que ingredient n'est pas vide (avec enter))
            ingredient = input("Ajoutez un ingrédient (ou ENTER pour terminer) : ")
            if ingredient == "":
                return
            self.ingredients.append(ingredient)
            print(f"Vous avez {len(self.ingredients)} ingrédient(s) : {', '.join(self.ingredients)}")

    def calculer_le_prix(self):
        prix = self.PRIX_DE_BASE + len(self.ingredients) * self.PRIX_INGREDIENT_SUPPLEMENTAIRE
        self.prix = prix


pizzas = [
    Pizza("4 fromages", 8.5, ("brie", "emmental", "compté", "parmesan"), True),
    Pizza("Napolitaine", 6.5, ("tomate", "basilic", "huile d'olive"), True),
    Pizza("Calzone", 10.5, ("tomate", "oignons", "viande")),
    Pizza("Super", 12, ("brie", "emmental", "oignons", "viande", "basilic")),
    Pizza("végétarienne", 9.5, ("champignons", "tomate", "oignons", "poivrons"), True),
    PizzaPersonnalisee(),
    PizzaPersonnalisee()
]

# pizza1 = Pizza("4 formages", 8.5, ("brie", "emmental", "compté", "parmesan"))
# pizza1.Afficher()

# for each in pizzas:  # affichage classique avec tous les informations
#     each.Afficher()

## Faire du tri - Filtrer:

# for each in pizzas:  # N'affiche que les pizzas végétariennes
#     if each.vegetarienne:
#         each.Afficher()

# for each in pizzas:  # N'affiche que les pizzas non végétariennes
#     if not each.vegetarienne:
#         each.Afficher()

# for each in pizzas:  # N'affiche que les pizzas avec de la tomate
#     if "tomate" in each.ingredients:
#         each.Afficher()

# for each in pizzas:  # N'affiche que les pizzas dont le prix est supérieur à 10 €
#     if each.prix >= 10:
#         each.Afficher()

#def tri(each):  # Tri avancé sur l'ordre alphabetique, le prix ou les ingrédients
    #return each.nom  #Tri par ordre alphabetique
    #return each.prix #Tri par prix
    #return len(each.ingredients) # Tri par le nombre d'ingrédients

#pizzas.sort(key=tri)  # permet de lancer le tri en faisant appel à la fonction "tri"; on peut cumuler avec le reverse=True

for each in pizzas:
    each.Afficher()

# ingredients = ("prie", "emmental", "compté", "parmesan")
# print(", ".join(ingredients))   # permet d'afficher la liste "ingrédient" sur une même ligne avec les objets séparés par ", "

