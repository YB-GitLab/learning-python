import sys

# print(sys)
print(sys.argv)

sys.argv[0] # nom de notre script

# Méthode avec les Slices
arguments = sys.argv[1:]

# Méthode avec l'unpacking avec l'astérisque
nom_script, *arguments = sys.argv

# l'utilisation de l'underscore nous permet d'éviter d'utiliser un arguments
_, *arguments = sys.argv
print(arguments)

# exemple de scrit qui réalise une addition

import sys
# extraction des arguments en ligne de commande
_, *arguments = sys.argv

operand1 = arguments[0]
operand2 = arguments[1]

print(operand1, operand2)
# print(operand1 + operand2) --> concatene car les deux arguments sont des str

operand1 = float(arguments[0])
operand2 = float(arguments[1])
# ici on converti les operandes en float
print(operand1 + operand2)
