# Déclaration des booléens
vrai = True
faux = False

print(vrai, type(vrai))
print(faux, type(faux))

print('--------------')
# utilisation de bool() si c'est différent de 0 c'est égale à True
a = 0
b = 0.0
c = 0.01
print('le bool de ',a, 'est ',bool(a))
print('le bool de ',b, 'est ',bool(b))
print('le bool de ',c, 'est ',bool(c))

print('-----------------')
# une chaîne de caractère vide ou une liste vide renvoie False
# ceci fonctionne aussi pour les dict et les set
txt = ''
liste = list()
dict = {}
set = set()

print(txt, 'chaine de caractère de type',type(txt))
print('le bool de ce string est ',bool(txt))

print(liste, 'liste de type',type(liste))
print('le boool de cette liste est ',bool(liste))

print(dict, 'dictionnaire de type',type(dict))
print('le bool de ce dictionnaire est ',bool(dict))

print(set, 'set de type',type(set))
print('le boole de ce set est ',bool(set))

# astuce
print('True + True =>', True + True)
#donnera 2
print('True + True + True =>', True + True + True)
#donnera 3
print('False + False =>', False + False)
#donnera 0

