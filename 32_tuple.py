# les tuple
mon_tuple = 1, 2, print, 4, 'Yo'
print(mon_tuple)
mon_tuple = (1, 2, print, 4, 'Yo')
print(mon_tuple)
print('---')


# unpacking de tuple comme a, b, c = x, y, z

mon_tuple = 'x', 'y', print ## ici nous avons l'emballage «PACKING»
print(mon_tuple, type(mon_tuple)) ##

a, b, c = mon_tuple ### ici on à le déballage «UNPACKING»
print(a, type(a)) ###
print(b, type(b)) ###
print(c, type(c)) ###
print('------')

# Déballage Partiel

mon_tuple = 1, 2, 3, 4, 5

a, *b = mon_tuple
print(a, type(a))
print(b, type(b))
print('-----')

# déballage partiel Avec 2 variables dans mon_tuple à l'emballage et 3 variables au déballage ??

mon_tuple = 1, 2
a, *b, c = mon_tuple
print(a)
print(b) # <- renvoie une liste vide
print(c)




