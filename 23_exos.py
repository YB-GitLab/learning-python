# String slicing

# Exercice 1 - Afficher "bcdefghijklmnopqrstuvwxyz"

alphabet = "abcdefghijklmnopqrstuvwxyz"

tranche = alphabet [1::]
print(tranche)

print('---------')

# Exercice 2 -Afficher "klmnopqrstuvwxyz"

alphabet = "abcdefghijklmnopqrstuvwxyz"

tranche = alphabet [10::]
print(tranche)

print('-----------')

# Exercice 3 -Afficher "abcde"

alphabet = "abcdefghijklmnopqrstuvwxyz"

tranche = alphabet[0:5:]

print(tranche)

print('-------------')
# Exercice 4 -Afficher "abcdefghijkl"

alphabet = "abcdefghijklmnopqrstuvwxyz"

tranche = alphabet[0:12:]

print(tranche)

print('----------')
# Exercice 5 -Afficher "defghijk"

alphabet = 'abcdefghijklmnopqrstuvwxyz'

tranche = alphabet[3:11:]
print(tranche)

print('---------')

# Exercice 6 -Afficher "tuvwx"

alphabet = "abcdefghijklmnopqrstuvwxyz"

tranche = alphabet[19:24]

print(tranche)

print('------------')

# Exercice 8 -Afficher "bcdefghijklmnopqrstuvwxy"

alphabet = "abcdefghijklmnopqrstuvwxyz"

tranche = alphabet[1:25:]
print(tranche)

print('------------')
# Exercice 9 -Afficher "cdefghijklmnopqrstuvwx"

alphabet = "abcdefghijklmnopqrstuvwxyz"

tranche = alphabet[2:24:]
print(tranche)

print('------------')

# Exercice 10 -Afficher "acegikmoqsuwy"

alphabet = "abcdefghijklmnopqrstuvwxyz"

tranche = alphabet[::2]
print(tranche)

print('---------')

# Exercice 11 -Afficher "adgjmpsvy"

alphabet = "abcdefghijklmnopqrstuvwxyz"

tranche = alphabet[::3]
print(tranche)

