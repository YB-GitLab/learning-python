# mots commun entre deux phrases

a = "bonjour je m'appelle toto"
b = "Bonjour, je suis titi"

def get_common_words(a, b):
    words_a = a.lower().replace(",", "").split(" ")
    words_b = b.lower().replace(",", "").split(" ")

    common_words = []

    for each in words_a:

        if each in words_b:
            common_words.append(each)

    return common_words


def get_common_words_2(a, b):
    words_a = set(a.lower().replace(",", "").split(" "))
    words_b = set(b.lower().replace(",", "").split(" "))

    #return list(words_a.intersection(words_b))
    return list(words_a & words_b)


print(get_common_words_2(a, b))
