# recherche dichotomique
# est beaucoup plus efficace que la recherche linaire mais sur des données déjà triées

from utils import generate_random_list

# l = generate_random_list(14, 0, 40)
# l.sort()
# print(l)

l = [7, 10, 11, 11, 18, 19, 22, 22, 25, 26, 28, 34, 37, 39]
# -> index
# -1 -> non trouvé

def binary_search(l, e, imin, imax):
    if imax-imin == 0:
        if l[imin] == e:
            return imin
        return -1
    if imax - imin == 1:
        if l[imin] == e:
            return imin
        if l[imax] == e:
            return imax
        return -1
    icenter = (imax+imin)//2
    if l[icenter] == e:
        return icenter
    if l[icenter] < e:
        return binary_search(l, e, icenter+1, imax)
    return binary_search(l, e, imin, icenter-1)

print(binary_search(l, 40, 0, len(l)-1))