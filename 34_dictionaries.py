# les Dictionnaires

#RAPPEL
# dico = dict()
dico = {}

print(type(dico))
print(bool(dico))
print(len(dico))

print('--------')

# le builtin hash
h1 = hash(0) #int
h2 = hash(0.0) # float
h3 = hash('Hello') # str
mon_tulpe = (1, 1.1, '')
h4 = hash(mon_tulpe)

print(h1, type(h1))
print(h2, type(h2))
print(h3, type(h3))
print(h4, type(h4))

# Ajouter une paire de clé / valeur
dico = {'clé1' : 1000,
        'clé2' : [100, 20],
        'clé3' : print} # ajouter des clés à la création (attention aux deux-points et à la virgule)
        # 'clé1' : "Hello"} # si on ajoute une clé qui est déjà dans le dictionnaire la nouvelle écrase l'ancienne

dico['cléX'] = "Yo" # rajoute en clé à l'éxécution
print(dico)
print('-------')

# pour lire un clé
print(dico['cléX']) # permet de lire la clé souhaitée

# pour modifier
dico['cléX'] = 'Yop'
print(dico['cléX'])

# pour supprimer
del dico['cléX']
print(dico)

print('------')
# les builtin «in» et «len()»

dico = {'a': 0, 'b': 1}
print('a' in dico) # avec «in» on en peut rechercher que des clés
print(len(dico))

print('--------')

# Accès sécurisé get() et pop()
dico = {'a': 0, 'b': 1}
# print(dico='c') # -> renvoi une erreur KeyError car il n'y a par de clé 'c'
dico.get('c', None) # lire la valeur associé à la clé en argument
dico.pop('c', -1) # lire puis supprimer la paire clé / valeur

print('------')
# items() keys() values()
dico = {'a': 0, 'b': 1}
resultat = dico.items()
print(type(resultat))
print(list(resultat))

resultat = dico.keys()
print(type(resultat))
print(list(resultat))

resultat = dico.values()
print(type(resultat))
print(list(resultat))
