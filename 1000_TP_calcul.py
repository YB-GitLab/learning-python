import random

NOMBRE_MIN = 1
NOMBRE_MAX = 10
NOMBRE_QUESTIONS = 3
moyenne = int(NOMBRE_QUESTIONS / 2)
nb_points = 0
operateur = ""

def choisir_operateur():
    o_rand = random.randint(1, 3)
    o_str = "+"
    if o_rand == 2:
        o_str = "-"
    elif o_rand == 3:
        o_str = "*"
    elif o_rand == 4:
        o_str = "/"
    return o_str

def calculer_resultat(a, b, o):
    resultat = 0
    if o == "+":
        resultat = a + b
    elif o == "-":
        resultat = a - b
    elif o == "*":
        resultat = a * b
    return resultat


def poser_question(nb_min, nb_max):
    a = random.randint(nb_min, nb_max)
    b = random.randint(nb_min, nb_max)
    o = choisir_operateur()

    resultat_utilisateur = input(f"Quel est le résultat de {a} {o} {b} = ")

    if calculer_resultat(a, b, o) == int(resultat_utilisateur):
        return True
    return False




for i in range(0, NOMBRE_QUESTIONS):
    print(f"Question N°{i+1}")
    if poser_question(NOMBRE_MIN, NOMBRE_MAX):
        print("Bravo")
        nb_points += 1
    else:
        print("Ce n'est pas correct")
    print()

print(f"Votre note est : {nb_points} / {NOMBRE_QUESTIONS}")
print()

if nb_points == NOMBRE_QUESTIONS:
    print(f"{nb_points} / {NOMBRE_QUESTIONS} -> excellent")
elif nb_points == 0:
    print(f"{nb_points} -> Révisez vos maths!")
elif nb_points > moyenne:
    print(f"Pas mal")
else:
    print("Peut mieux faire")
