# Compter les majuscules
#
"""
def count_upper_characters_1(str):
    count = 0
    for each in str:
        if each.isupper():
            count += 1
    return count


s = "Bonjour Toto"

print(count_upper_characters(s))
"""

def count_upper_characters_2(s):
    # Première façon de faire
    """
    liste = [1 if each.isupper() else 0 for each in s]
    i = liste.count(1)
    i = sum(liste)
    return i
    """
    # Seconde façon de faire
    liste = [1 for each in s if each.isupper()]  # fonctionne de la même manière qu'au dessus mais de manière un peu plus simplifié et rempli la liste qu'avec les 1 et sans les 0
    return len(liste)  # comme la liste ne contient que des 1, on peut juste calculer sa longueur pour avoir le résultat (cela évite au programme de rentrer dans la liste pour calculer), ce qui fait gagner du temps machine
s = "Bonjour TOTO"

print(count_upper_characters_2(s))