# les String Methods

texte = "les 4 méthodes à connaitre"

print("original ->", texte)
print("upper ->", texte.upper())
print("lower ->", texte.lower())
print('-----------')
# method index()
recherche = "méthodes"
index = texte.index(recherche)
print(index, type(index)) # renvoi un int qui donne le nombre de caractère avant d'avoir trouver la recherche
# index est sensible à la casse
index = texte.lower().index(recherche)
print(texte[index])
print('---------')
# method strip()
texte_strip = "    j.p@mail.net                "
base_de_données = "j.p@mail.net"

print(texte_strip.strip() == base_de_données)
print(texte_strip)
print(texte_strip.strip())



