## Les Callbacks

# def ma_fonction():
#     print("Toto")
#     return 1
#
# a = 5
# b = ma_fonction()
#
# print("a", a, "b", b)

# def afficher_table_multiplication(n):
#     for i in range(1, 10):
#         print(i, "x", n, "=", i*n)
#
# afficher_table_multiplication(2)
def mult_callback(a, b):  # on a créer une fonction que fait ce qu'on veut, qui sera utilisée par le callback
    return a*b


def add_callback(a, b):
    return a+b


def afficher_table(n, operateur_str, operation_cbk):  # on rajoute une variable_cbk
     for i in range(1, 10):
         print(i, operateur_str, n, "=", operation_cbk(i, n))  # on appelle ici la variable avec les (a, b)


afficher_table(2, "x", mult_callback)  # on appelle ici la bonne fonction
print()
afficher_table(2, "+", add_callback)

