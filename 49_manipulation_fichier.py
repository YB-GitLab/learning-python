# Manipuler les fichiers

# Ici, si notre fichier ne se trouvera pas à la racine (main.py)
# from pathlib import Path
# dossier = Path('.')
# nom_fichier = 'journal.txt'
# chemin_vers_votre_fichier = dossier / nom_fichier
# with open(chemin_vers_votre_fichier)...

# ici, on travail directement à la racine (main.py)
# Code avec WRITE
# with open('journal.txt', 'w') as fichier:  # le 'w'
#     #print(fichier, type(fichier))
#     fichier.write('Hello world\n')
#     fichier.write('Hello\n')
#
# # Code avec APPEND
# with open('journal.txt', 'a') as fichier:  # le 'a'
#     #print(fichier, type(fichier))
#     fichier.write('Hello world\n')
#     fichier.write('Hello\n')

# Code avec READ
#with open('journal.txt', 'r') as f:  # le 'r'
    # print(f.read())
    # contenu = f.read()
    # print(contenu)
    # print('#' * 15)

    # resultat = f.read()  # <-- tout
    # print(resultat)
    # print(type(resultat))
    # print(len(resultat))
    # print('#' * 15)

    # resultat = f.readlines()  # <-- toutes les lignes
    # print(resultat)
    # print(type(resultat))
    # print(len(resultat))

    # print('#' * 15)
    # resultat = f.readline()  # <-- ligne par ligne
    # print(resultat)
    # print(type(resultat))
    # print(len(resultat))

with open('poesie.txt', 'r') as f:
    contenu = f.readlines()
# les nouvelles lignes à rajouter au début
titre = 'Alfred de Musset - A Aimée d\'Alton\n'
separateur = '~' * (len(titre) - 1) + '\n'
# print(titre)
# print(separateur)
new_lines = [titre, separateur]
contenu = new_lines + contenu
# écraser l'ancien poème avec le nouveau
with open('poesie.txt', 'w') as f:
    f.writelines(contenu)
