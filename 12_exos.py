# Exos sur les poérateurs de comparaisons

# Exercice 1 - Tester chacun des opérateurs de comparaison
# Ecrivez un script qui pour chaque opérateur de comparaison contient une expression qui sera False et une expression qui sera True

print("Vrai", 0 == 0)
print("Faux", 1 == 0)

print("Vrai", 0 != 1)
print("Faux", 0 != 0)

print("Vrai", 0 < 1)
print("Faux", 2 < 1)

print("Vrai", 1 > 0)
print("Faux", -1 > 0)

print("Vrai", 1 >= 0)
print("Faux", -1 >= 0)

print("Vrai", 0 <= 1)
print("Faux", 1 <= 0)

print('----------------')

# Exercice 2 - Les comparaisons chaînées
# Que va afficher le script suivant? Recopiez ensuite le code pour vérifier votre réponse.
a = 1 < 2 < 3 # True
b = 1 <= 2 <= 3 # True
c = 2 <= 2 <= 3 <= 3 # True

print(a, b, c)

# Exercice 3 - Texte & égalité
# Une seule des ces expressions sera évaluée à True, pouvez vous deviner la quelle?

a = "texte" == "TEXTE"
b = "texte" == "texte" # celle-ci
c = " texte " == "texte"
d = "Texte" == "texte"

print(a, b, c, d)

print('----------------')
# Exercice 4 - Modulo et test de parité
# ajouter des commentaires pour clarifier chacune des lignes de code.

# Ici on test si la variable nombre est pair ou impair
# assignation de variables
nombre = 10
# assignation de variables + comparaison d'égalité de modulo 2 avec soit 0 (pair) soit 1 (impair)
test_impaire = nombre % 2 == 1
test_paire = nombre % 2 == 0

print(nombre, 'pair?', test_paire)
print(nombre, 'impair?', test_impaire)

print('-----------')
# Exercice 5 - Résoudre un problème d'imprécision
# ici, deux formules pour le même calcul ?, peut-être qu'à cause de l'imprécision des float, ce ne sera pas égale ?
formule_1 = 10.0 / 2 * 3
formule_2 = 10.0 / (2 * 3)
# cela va afficher false
print(formule_1)
print(formule_2)
print(formule_1 == formule_2)
# une solution existe avec Epsilon -epsilon < a - b < epsilon
# si c'est true on considère alors que a et b sont égaux
epsilon = 0.00000001

print(-epsilon < formule_1 - formule_2 < epsilon)

