# Les sets pour manipuler des éléments distincts

# Exercice 1 - Les sets pour compter les éléments distincts d'une liste.

journal_dappel = ['Alice', 'Bob', 'Alice', 'Carla', 'Denzel', 'Alice', 'Bob', 'Carla', 'Denzel']
contacts_frequents = set(journal_dappel)

print(journal_dappel) # va afficher la liste journal d'appel
print(contacts_frequents) # va afficher le set de la liste journal d'appel

n_contacts = len(contacts_frequents)
print(f'J\'appelle souvent {n_contacts} personnes différentes') # va afficher le nombre de personnes uniques soit 4

# Exercice 2 - Vérifier si un élément est dans un set

contacts_frequents = {'Denzel', 'Bob', 'Alice', 'Carla'}
tests_dinclusion = 'Parents' in contacts_frequents
print('Est ce que j\'appelle souvent mes parents?', tests_dinclusion) # réponse négative False

# Exercice 3 - À votre tour
contacts_frequents = {'Denzel', 'Bob', 'Alice', 'Carla'}
tests_dinclusion = 'Bob' in contacts_frequents
print('Est ce que j\'appelle souvent Bob?', tests_dinclusion)

# Exercice 4 - Ajouter plusieurs fois le même élément
contacts_frequents = {'Denzel', 'Bob', 'Alice', 'Carla'}
contacts_frequents.add('Parents')
contacts_frequents.add('Parents')
contacts_frequents.add('Parents')
contacts_frequents.add('Parents')
contacts_frequents.add('Parents')
tests_dinclusion = 'Parents' in contacts_frequents
print('Est ce que j\'appelle souvent mes parents?', tests_dinclusion) # pas d'erreur et va afficher True
print(contacts_frequents)

# Exercice 5 - Tenter de supprimer plusieurs fois le même élément

contacts_frequents = {'Carla', 'Denzel', 'Bob', 'Alice', 'Parents'}
contacts_frequents.remove('Bob')
tests_dinclusion = 'Bob' in contacts_frequents
print('Est ce que j\'appelle souvent Bob ?', tests_dinclusion) # va afficher False et pas d'erreur
print(contacts_frequents)