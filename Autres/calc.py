import sys
# lecture des arguments
script_name, mode, *arguments = sys.argv

mode = str(mode)
mode = mode.lower()
modes = ['+', '-', 'x', '/', '%', 'exp', '//']
aide = "toute l'aide nécessaire"
aide_erreur = f'Veuillez consulter l\'aide en tapant : {script_name} aide'

if (mode == 'aide') or (len(arguments) != 2):
    if mode == 'aide':
        print(aide)
        exit()
    else:
        print('Mauvais nb arg')
        print(aide_erreur)
        exit()

# Fonctionne pour tester operande / float
# try:
#     operande_1 = float(arguments[0])
# except ValueError:
#     print('Operande1 non float')
#     exit()
# try:
#     operande_2 = float(arguments[1])
# except ValueError:
#     print('reussi2')
#     exit()

operande_1 = arguments[0]
operande_2 = arguments[1]

# remplacer des virgules par des points
operande_1 = operande_1.replace(',', '.')
operande_2 = operande_2.replace(',', '.')
# on compte le nombre de points (on en veut 1 ou 2)
# ... puis on vérifie que tout est numérique
if operande_1.count('.') in [0, 1] and operande_1.replace('.', '').isdigit():
        operande_1 = float(operande_1)
else:
    print(f'Échec, {operande_1} comporte une erreur')
    exit()
if operande_2.count('.') in [0, 1] and operande_2.replace('.', '').isdigit():
    operande_2 = float(operande_2)
else:
    print(f'Échec, {operande_2} comporte une erreur')
    exit()

if mode in modes:
    if mode == modes[0]:
        resultat = operande_1 + operande_2
        symbole = '+'
        resu = 'La somme'
    if mode == modes[1]:
        resultat = operande_1 - operande_2
        symbole = '-'
        resu = 'La différence'
    if mode == modes[2]:
        resultat = operande_1 * operande_2
        symbole = 'x'
        resu = 'Le produit'
    if mode == modes[3]:
        resultat = operande_1 / operande_2
        symbole = '/'
        resu = 'Le quotient'
    if mode == modes[4]:
        resultat = operande_1 % operande_2
        symbole = '%'
        resu = 'Le modulo'
    if mode == modes[5]:
        resultat = operande_1 ** operande_2
        symbole = '**'
        resu = 'La puissance'
    if mode == modes[6]:
        resultat = operande_1 // operande_2
        symbole = '//'
        resu = 'Le divisionfloor'

    conclusion = f'{resu} de {operande_1} {symbole} {operande_2} est égale à {resultat}'
    print(conclusion)
    exit()
else:
    print(f'Veuillez consulter l\'aide en tapant : {script_name} aide')
    exit()
