import sys


nom_script, *arguments = sys.argv

# defini un message d'aide'

aide = f"""bienvenue, voici la syntaxe {nom_script}
<operateur> <operande_1> <operande_2"""
# d_aide = str(arguments)
# d_aide = d_aide.lower()
#
# if d_aide == 'aide':
#     print(aide)
#     exit()
# elif d_aide != '+' or '-' or '*' or '/':
#     print(aide)
#     exit()
# elif d_aide == '':
#     print(aide)
#     exit()

if len(arguments) != 3:
    print(aide)
    exit()

operateur, operande_1, operande_2 = arguments
operande_1 = float(operande_1)
operande_2 = float(operande_2)

if operateur == 'aide':
    print(aide)
    exit()

elif operateur == '+' or '-' or '/' or '*':
    print(f'operateur selectionné {operateur}')
else:
    print('choix possible : + - / *')
    exit()
if operateur == '+':
    resultat = operande_1 + operande_2
elif operateur == '-':
    resultat = operande_1 - operande_2
elif operateur == '*':
    resultat = operande_1 * operande_2
else:
    resultat = operande_1 / operande_2
print(f'Le résultat de {operande_1} {operateur} {operande_2} est {resultat}')

