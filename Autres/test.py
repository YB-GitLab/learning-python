symbole = 'A'
repet = 3
mi = 0
hauteur = 10
for nb in range (repet):
    if nb == 1:
        mi = hauteur / 2
    for i in range(int(mi), int(hauteur)):
        taille_vide = int(hauteur - 1 - i)
        taille_bloc = 2 * i + 1
        ligne = ' ' * taille_vide + symbole * taille_bloc
        print(ligne)
    mi = mi + nb