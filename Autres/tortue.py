import turtle

t = turtle.Turtle()


def escalier(avance, angle, nombre_marche):
    for i in range(nombre_marche):
        t.forward(avance)
        t.left(angle)
        t.forward(avance)
        t.right(angle)
    t.forward(avance)

def carre(avance, angle=90):
    for i in range(4):
        t.forward(avance)
        t.left(angle)

def carres(nombre_carre, avance, angle=90):
    for i in range(nombre_carre):
        carre(avance, angle=90)
        avance = avance / 2
# escalier(30, 90, 5)
# carre(100)
carres(5, 600)


turtle.done()
