# l'outil Path

from pathlib import Path
# print(Path)
#
ici = Path('.')
#print(ici)
# print(ici.absolute())

#print(list(ici.iterdir()))

# chemin_dossier2 = ici / 'dossier_2'
# chemin_fichierA = ici / 'dossier_1/fichier_A.txt'
# print(chemin_dossier2)
# print(chemin_dossier2.exists())
# print(chemin_fichierA.exists())
#
# print(list(chemin_dossier2.iterdir()))

# for chemin in ici.iterdir():
#     print(chemin, '\nDIR ?', chemin.is_dir(), '\nFILE?', chemin.is_file())


# Renommer Créer Supprimer
dossier_A = (ici
             / 'dossier_1'
             / 'dossier_A')

chemin_fichier_C = dossier_A / 'fichier_C.txt'
chemin_fichier_D = dossier_A / 'fichier_D.txt'
print(chemin_fichier_C)
print(chemin_fichier_C.exists())

print(dossier_A.exists())
print(dossier_A.absolute())
chemin_fichier_C.touch()

#chemin_fichier_C.rename(chemin_fichier_D)

#chemin_fichier_D.unlink()

