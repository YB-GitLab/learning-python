
# simples quotes pour déclarer la chaine de caractère, avec des doubles quotes dans celle-c
texte_dq = 'des "doubles quotes"!'
print(texte_dq, type(texte_dq))

# l'inverse fonctionne aussi
texte_sq = "des 'simples quotes'!"
print(texte_sq, type(texte_sq))

print('--------')
#On peut convertir des nombres en string avec str()

a = 10.45
print(a, "type d'origine :", type(a))
print(a, 'type après str() :', type(str(a)))

b = 9.332
tstr = str(b)
print(b, "type d'origine :", type(b))
print(tstr, "type après str() :", type(tstr))


print('--------------------')
# on ne peut pas concatener un <int> ou <float> avec un <str>

nb = 50
txt = str(nb)
print(txt, type(txt))
# print(txt + 'nb) # --> ne fonctionne pas car on ne peut pas concatener un <int> ou <float> avec un <str>

# par contre on peut concatener deux str ensembles

txt2 = 'chaines string supp'
print(txt + ' ' + txt2)

print('-------------------')
# utilisation du '\' (backslash) qui permet d'échapper la fonction d'un caractère

txt = 'utilisation du \'\\\' avec de simples quotes'
txt2 = "utilisation du \"\\\" qui échape les doubles quotes"
print(txt, type(txt))
print(txt2, type(txt2))

# utilisation de '\' pour inérer des caractères tel que \n

dialogue = 'Elle : "salut"\nLui: "Hi"'
print('Utilisation du \\n :')
print(dialogue)

print('----------------')
# utilisation des triples quotes qui permet de documenter le code sur plusieurs lignes

txt = '''les triples cotes permettent
d'écrire
sur plusieurs
lignes


avec deux sauts de ligne'''

print(txt, type(txt))
