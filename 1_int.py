# le type <int>
# assignement de variable (introduction)
# Fonction introduction
# les opérations mathématiques
# int + int = int
# int - int = int
# int * int = int
# int / int = float

# variables de type int
a = 10
b = 20

print(a + b, type(a + b))
print(a - b, type(a - b))
print(a * b, type(a * b))
print(a / b, type(a / b))
