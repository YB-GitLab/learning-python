## Nombre variable d'arguments'

def somme(*nombres):  # ici on peut mettre une wildcard qui permet d'avoir un nombre variable de arguments
# def somme(arg1, *nombres):  # on peut aussi passer des arguments en premier
# def somme(ang1, **nombres):  # on peut mettre deux wislcard, c'est le même principe sauf que ça créera un dictionnaire
    resultat = 0
    for n in nombres:
    # for n in nombres.values():  # avec deux wildcards, on travaille avec un dico
        resultat += n
    return resultat


print(somme(5, 7, 8, 21))  # ici on peut mettre autant d'arguments que l'on souhaite
print(geo=12, math=13, sport=6)  # avec une double wildcard, on travaille avec des dictionnaires

