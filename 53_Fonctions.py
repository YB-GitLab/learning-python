## Les fonctions

# def nom_de_la_variable():
#     print('Hello World')

# nom_de_la_variable()  # permet de lancer la fonction

## On peut importer une fonction se trouvant dans un autre fichier

#from helper import afficher_aide
# from helper import dire_bonjour
# #print(afficher_aide())
#
# def dire_bonjour_2_fois():
#     dire_bonjour()
#     dire_bonjour()
#
# print(dire_bonjour_2_fois())

# ##Travailler avec Return
# def ma_fonction_avec():
#     print('Hey Avec')
#     return 'Valeur de retour'
#
# def ma_fonction_sans():
#     print('Hey Sans')
#
# avec = ma_fonction_avec()
# sans = ma_fonction_sans()
#
# print(avec)
# print(sans)

## passage d'arguments

# def ma_fonction():
#     return 'Hello'
#
#
# valeur_de_retour = ma_fonction()
#
# print(valeur_de_retour)

## passage d'arguments positionels

# def dire_qqch(qqch):  # passage de la l'argument positionnel
#     print(qqch)       # on l'affiche

# dire_qqch('Hey You')  # ici, on déclare la valeur de l'argument

## passage d'un argument par défaut (soit par Keyword)

# def dire_qqch(qqch="quelquechose par defaut"):  # l'argument par défaut est ="quelquechose"
#     print(qqch)
#
#
# dire_qqch()

## passage d'arguments par Keywords (soit par defaut)

# def addition(a, b=5, c=0):
#     return a + b + c
#
#
# result = addition(10,20,30)
# print(result)
# result = addition(12, 13)
# print(result)
# result = addition()
# print(result)
