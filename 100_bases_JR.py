def demander_nom():
    nom_reponse = ""
    while not nom_reponse:
        nom_reponse = input("\nQuel est votre nom ? ")
    return nom_reponse


def demander_age(nom_peronne):
    age_int = 0
    while age_int == 0:
        age_str = input(nom_peronne + ", quel est votre âge ? ")
        try:
            age_int = int(age_str)
        except:
            print("Erreur: Veuillez entrer un nombre")
        return age_int


def afficher_information_personne(nom, age, taille=0):
    print()
    print(f"Vous vous appellez {nom}, vous avez {age} ans")
    print(f"L'an prochain vous aurez {age + 1} ans")
    senior = age > 60
    majeur = age > 18
    majeur_18 = age == 18
    mineur_17 = age == 17
    enfant = age < 10
    adolescent = 12 <= age < 18

    if senior:
        print("Vous êtes senior")
    elif majeur:
        print("Vous êtes majeur")
    elif majeur_18:
        print("Vous êtes tout juste majeur")
    elif mineur_17:
        print("Vous êtes presque majeur")
    elif adolescent:
        print("Vous êtes adolescent")
    elif enfant:
        print("Vous êtes enfant")
    else:
        print("Vous êtes mineur")

    # afficher taille
    if not taille == 0:
        print("Vous mesurez " + str(taille) + "m")


# demander le nom
#nom1 = demander_nom()
#nom2 = demander_nom()

# demander l'âge'
#age1 = demander_age(nom1)
#age2 = demander_age(nom2)

# afficher les résultats
#afficher_information_personne(nom1, age1)
#afficher_information_personne(nom2, age2)

# Automatiser le nombre de personnes

nb_personne = input('Combien de personnes ? ')


for i in range(0, int(nb_personne)):
    nom = demander_nom()
    age = demander_age(nom)
    afficher_information_personne(nom, age)



