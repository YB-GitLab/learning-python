## Les subtilités de la scope et des objets modifiables
from pcom import pcom
# Exercice 1 - Avez vous appris la Règle n°1 des scopes et des fonctions?

def creer_variable():
    nouvelle_variable = 'variable intérieur'


creer_variable()
# print(nouvelle_variable) # Toute variable définie à l'intérieur d'une fonction n'existe pas à l'extérieur de cette fonction
pcom()

# Exercice 2 - Avez vous appris la Règle n°2 des scopes et des fonctions?

NE_PAS_MODIFIER = 'fichier top secret'

def une_autre_fonction():
    NE_PAS_MODIFIER = 'Bonne recette'
    print("j'adore'")
    print("J'espère ne jamais la perdre. \n")


une_autre_fonction()  # va afficher : j'adore
                                     #j'espère ne jamais la perdre
print('Ne pas faire de modif', NE_PAS_MODIFIER)  # va afficher : ne pas faire de modif fichier top secret

pcom()
# Exercice 3 - Avez vous bien appris la première partie de la Règle n°3 des scopes et des fonctions?

NE_PAS_MODIFIER = 'Fichier top secret'

def lire_le_rapport():
    print('jetons un oeil, à la variable externe')
    print('Je lis:', NE_PAS_MODIFIER)
    print('Ok')

lire_le_rapport()

pcom()

# Exercice 4 - Avez vous bien appris la suite de la Règle n°3 des scopes et des fonctions?

NE_PAS_MODIFIER = 'Fichier top secret'

def corrompre_le_fichier():
    print('Jetons un oeil à ce rapport secret, défini dans le scope externe')
#    print("je lis le :", NE_PAS_MODIFIER)  ## Il est possible d'accéder aux variables définies à l'exterieur d'une fonction dans la scope interne de cette fonction... si et seulement si je n'essaie pas de modifier cette variable par la suite dans cette même fonction
    print('... Personne ne doit-être au courant. Je vais le modifier pour que personne le sache')
    NE_PAS_MODIFIER = " .. ... ... Fichier corrompu ... ... .."


corrompre_le_fichier()

pcom()

# Exercice 5 - Attention quand vous passez des listes en arguments

from helper import afficher_a_lenvers

ne_pas_modifier = [1, 2, 3, 4, 5]

afficher_a_lenvers(ne_pas_modifier)

print('\nJetons un oeil à ma précieuse liste à ne pas modifier')
print(ne_pas_modifier)

pcom()
# Exercice 6 - Attention quand vous passez des dictionnaires en arguments

from helper import afficher_dico_clé_maj

dico_ne_pas_modif = {'clé': 123,
                     'clé2': 1234,
                     'clé3': 12345}

afficher_dico_clé_maj(dico_ne_pas_modif)

print('\nVérifions que le dico soit intacte')
print(dico_ne_pas_modif)

pcom()

# Exercice 7 - Exploiter les références vers des objets modifiables pour contourner la Règle n° 3

compteur_dappel = [0]

def fonction_a_compter():

    compteur_dappel[0] += 1

    print('hey')


fonction_a_compter()
fonction_a_compter()

print(compteur_dappel)

fonction_a_compter()
fonction_a_compter()

print(compteur_dappel)