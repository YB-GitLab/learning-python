# Trouver les éléments manquants
#
# Entre " et 10 (inclus)

a = [3, 8, 2, 4, 7]


def trouver_elements_manquants_1(a, min, max):
    les_manquants = []
    for i in range(min, max + 1):
        if not i in a:
            les_manquants.append(i)
    return les_manquants


def trouver_elements_manquants_2(a, min, max):
    return [i for i in range(min, max+1) if not i in a]

print(trouver_elements_manquants_2(a, 1, 10))