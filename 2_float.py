# variables de type Float
A = 2.57
B = 2.00
E = .90
F = 0.

# on peut aussi les écrire en notation scientifique
G = 3e3
H = 98e-2

print(A, type(A))
print(B, type(B))
print(E, type(E))
print(F, type(F))
print(G, type(G))
print(H, type(H))

print('--------------')
# quand on fait des maths avec des floats et des ints, le résultat sera toujours des floats

n_entier = 10
n_virgule = 0.5

print(n_entier, type(n_entier))
print(n_virgule, type(n_virgule))

somme = n_entier + n_virgule
différence = n_entier - n_virgule
produit = n_entier * n_virgule
quotient = n_entier / n_virgule

print(somme, type (somme))
print(différence, type(différence))
print(produit, type(produit))
print(quotient, type(quotient))

print('---------')

# Convertion des ints en floats

a = 10

print(a, type(a))

a = float(a)

print(a, type(a))

print('--------')
# convertion des floats en int … attention le résultat sera tronqué

a = 10.5

print(a, type(a))

a = int(a)

print(a, type(a))

print('---------')
#on peut arrondir avec la fonction round() --> floats en int arrondi

print(type(round(4.2)))