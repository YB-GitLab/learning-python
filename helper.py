# def afficher_aide():
#     print("toute l'aide")

def dire_bonjour():
    print('Bonjour')


def dire_qqch_Xfois(qqch, X):
    for _ in range(X):
        print(qqch)


def creer_phrase(mot1, mot2, mot3, mot4='', mot5=''):
    phrase = mot1.title() + ' ' + mot2.lower() + ' ' + mot3.lower()
    if mot4:
        phrase += ' ' + mot4.lower()
    if mot5:
        phrase += ' ' + mot5.lower()
    phrase += '.'
    print(phrase)
    return phrase

def afficher_a_lenvers(liste):
    while liste:
        dernier_element = liste.pop()
        print(dernier_element)

def afficher_dico_clé_maj(dico):
    clé_pas_en_maj = list(dico.keys())
    for k in clé_pas_en_maj:
        v = dico.pop(k)
        dico[k.upper()] = v
    print(dico)


