# 1 - importer les arguments
import sys

_, *arguments = sys.argv

if not arguments:
    print("Pas d'arguments inclus")
    exit()

# déclaration des variables scape, sym, (et autres suivant la forme)
# définition des arguments
# print(arguments[0])
# print(arguments[1])
# print(arguments[2])

forme = arguments[0].lower()
symbole = arguments[1]
longueur_base = int(arguments[2])
# hauteur_ou_repetition = float(arguments[3])
# sym_sup = arguments[4]

# Autres variables
scape = ' '
count_scape = 0  # int(x // 2 + x)
# tronc = f'{sym_sup} {sym_sup}'
count_scape_tronc = 0  # (x // 2 + x - 2)

# vérification de la forme et du nombre d'arguments avec la forme

nb_arguments = {'carré': 3,
                'carré_vide': 3,
                'triangle': 4,
                'triangle_iso': 3,
                'pyramide': 3,
                'rectangle': 4,
                'rectangle_vide': 4,
                'sapin_+': 6}

if forme not in nb_arguments.keys():
    print("La forme n'est pas répertorié")
    exit()
elif len(arguments) != nb_arguments.get(forme):
    print('Pb nb_arguments', len(arguments), '// attendu', nb_arguments.get(forme))
    exit()

# Codage des formes
if forme == 'carré':
    for i in range(1, longueur_base):
        print(symbole * longueur_base)
if forme == 'carré_vide':
    longueur_vide = longueur_base - 2
    for i in range(1, longueur_base + 1):
        if i == 1 or i == longueur_base:
            print(symbole * longueur_base)
        else:
            print(symbole + scape * longueur_vide + symbole)

if forme == 'rectangle':
    hauteur_ou_repetition = int(arguments[3])
    for i in range(hauteur_ou_repetition):
        print(symbole * longueur_base)
if forme == 'rectangle_vide':
    hauteur_ou_repetition = int(arguments[3])
    hauteur_vide = hauteur_ou_repetition - 2
    longueur_vide = longueur_base - 2
    for i in range(1, hauteur_ou_repetition + 1):
        if i == 1 or i == hauteur_ou_repetition:
            print(symbole * longueur_base)
        else:
            print(symbole + scape * longueur_vide + symbole)

if forme == 'triangle':
    hauteur_ou_repetition = int(arguments[3])
    for i in range(1, hauteur_ou_repetition + 1):
        print(symbole * i)
if forme == 'triangle_iso':
    for i in range(1, longueur_base + 1):
        print(symbole * i)

if forme == 'pyramide': # La bonne formule est base = hauteur * 2 - 1 // le nombre d'espace est: hauteur - 1 - i
    count_scape = longueur_base
    for i in range(5, longueur_base + 1):
        if i == 1:
            print(scape * (count_scape - i + 1) + symbole)
        print(scape * (count_scape - i) + symbole * (i + i + 1))

    # meilleur formule
    # for i in range(hauteur):
    #     taille_vide = hauteur - 1 - i
    #     taille_bloc = 2 * i + 1
    #     ligne = ' ' * taille_vide + symbole * taille_bloc
    #     print(ligne)

if forme == 'sapin_+':
    pass

print('end')

# 2.2 réalisation de la forme
# 3 - affichage de la forme
