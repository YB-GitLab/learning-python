# Tri par selection
import random

#l = [5, 8, 10, 2, 1, 6, 3, 2, 7]

#  V
# [1, 5, 8, 10, 2]

#     V
# [1, 2, 5, 8, 10]

#        V
# [1, 2, 5, 8, 10]

#           V
# [1, 2, 5, 8, 10]

#              V
# [1, 2, 5, 8, 10]

def generate_random_list(n, min, max):
    l = []
    for i in range(n):
        e = random.randint(min, max)
        l.append(e)

    return l


def selection_sort(l):
    for unsorted_index in range(0, len(l)-1):
        min = l[unsorted_index]
        min_index = unsorted_index
        for i in range(unsorted_index+1, len(l)):
            if l[i] < min:                    # on trouve le plus petit
                min = l[i]
                min_index = i                 #  V            m
        l[min_index] = l[unsorted_index]  # [5, 8, 10, 2, 5] on déplace le 5 qui est en position 1 à la fin de la liste
        l[unsorted_index] = min           # [1, 8, 10, 2, 5] on change le 5 de la position 1 par le 1

l = generate_random_list(100, -1000, 1000)
#print("unsorted: ", l)
selection_sort(l)
print("sorted: ", l)