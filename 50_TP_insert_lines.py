with open ('poesie.txt', 'r') as f:
    contenu = f.readlines()

separateur = ['~' * len(contenu[1]) + '\n']

contenu = contenu[:6] + separateur + contenu[7:]

with open ('poesie_TP.txt', 'w') as f:
    f.writelines(contenu)