import sys

_, billet, prix = sys.argv

billet = float(billet)
prix = float(prix)

if prix > billet:
    print(f'pas assez, il manque {prix - billet}€')
elif prix == billet:
    print('Somme exact')
else:
    trop = billet - prix
    print(f'Doit rendre {billet - prix}€')
    pieces = [2, 1, .5, .2, .1, .05, .02, .01]
    a_rendre=[]
    piece = pieces.pop(0)
    while trop != 0:
        if piece > trop:
            piece = pieces.pop(0)
        else:
            a_rendre.append(piece)
            trop = trop - piece
            # ici, on arrondi à deux nombres aprés la virgule
            trop = round(trop, 2)
    print((f'Soit {a_rendre}'))






