# on recupère chaque ligne dans une liste
with open ('poesie.txt', 'r') as f:
    contenu = f.readlines()
# on créé une nouvelle liste
new_contenu = []
# Pour chaque ligne, on les mouline avec upper pour les ajouter à la nouvelle liste
for each in range(len(contenu)):
    new_contenu.append(contenu[each].upper())
# on ecrit un fichier avec le contenu de la nouvelle liste
with open ('poesie_TP_maj.txt', 'w') as f:
    f.writelines(new_contenu)

## Autre solution
# with open('poesie.txt', 'r') as f:
#     contenu = f.read() # -- > ici on lit tout en bloc pour faire une <str>
#
# contenu = contenu.upper()
#
# with open('poesie_TP_maj.txt', 'w') as f:
#     f.write(contenu)