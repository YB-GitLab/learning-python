# Opérations Arithmétiques et Priorité

# Exercice 1 - Priorité Multiplication, Addition

a = 1 + 2 * 3 # 7
b = 1 * 2 + 3 # 5
c = (1 + 2) * 3 # 9
d = 1 * (2 + 3) # 5

print(a, b, c, d)

print('-----------')

# Exercice 2 - Priorité Division, Multiplication

a = 10 / 5 * 2 # 4
b = 10 / (5 * 2) # 1
print(a, b)

print('----------')

# Exercice 3 - Floor Division

a = 6 // 4 # 1
b = 6 / 4 # 1.5
c = -6 // 4 # -2
d = -6 / 4 # -1.5
print(a, b, c, d)
print(type(a), type(b)) # int et float
print(type(c), type(d)) # int et float

print('---------')

# Exercice 4 - Priorité Exposant, Unaire, Addition

a = 2 ** 2
b = 2 ** 2 + 2
c = -2 ** 2 + 2
print(a, b, c) # 4 6 -2

print('---------')

# Exercice 5 - Priorité Exposant, Soustraction, Parenthèses

a = (2 - 3) ** 2
b = 2 - 3 ** 2
c = 2 - (-3) ** 2
print(a, b, c) # -1 -7 -7

print('-----------')

# Exercice 6 - Modulo

a = 10 % 2
b = 11 % 2

print(a, b) # 0 1

print('----------')

# Exercice 7 - Modulo (suite..)

a = 50 % 5 # 0
b = 50 % 2 # 0
c = 50 % 10 # 0
d = 50 % 25 # 0
e = 50 % 30 # 20
f = 50 % 3 # 2
print(a, b, c, d, e, f)

print('----------')

# Exercice 8 - Passage de Minuit

heure_de_départ = 21
durée_du_voyage = 7
heure_darrivée = (heure_de_départ + durée_du_voyage) % 24

print('Si on part à',  heure_de_départ, 'heures et qu\'on arrive', durée_du_voyage, ' heures après alors…')
print('Il sera', heure_darrivée, 'heures.')

print('-----------')

# Exercice 9 - Un casse-tête

a = 0
b = 1
c = 2

casse_tête = b / c * a + b * (a + c) # 2
print(casse_tête)

print('----------')

# Exercice 10 - Encore plus de casse-têtes

casse_tête_1 = (2 // 1 + 1) * (2 // 3) # 0
casse_tête_2 = 2 // 1 + 1 * (2 // 3) # 2
print(casse_tête_1)
print(casse_tête_2)

print('----------')

# Exercice 11- Un dernier pour la route

le_dernier_casse_tête = - 2 ** 2 // 2 * 2 % 2 + (1 + 1) # 2
print(le_dernier_casse_tête)