# Utilisation de split() --> permet de créer une liste à partir d'une str
chaîne = "Lundi Mardi Mercredi Jeudi Vendredi"
print(chaîne)
liste = chaîne.split(' ')
print(liste)
chaîne2 = ' '.join(liste)
print(chaîne2)