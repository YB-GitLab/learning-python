# Floor division

a = 2//3
print(a)

# Vérification int et float
print('--------')

entier = 3
virgule = 2.0

print(entier // entier, type(entier // entier))
print(entier // virgule, type(entier // virgule))
print(virgule // entier, type(virgule // entier))
print(virgule // virgule, type(virgule // virgule))

print('-------------')
# Vérification avec négation

nombre1 = -3
nombre2 = 2

print(nombre1 // nombre2)
print(nombre2 // nombre1)

print('--------')
# Vérification entre positif et négatif

print(3//2)
print(-3//2)
