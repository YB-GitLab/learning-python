# IF (structure conditionnelle)

import sys

# récupère les arguments depuis la ligne de commande
# _, *arguments = sys.argv
# billet, prix = arguments

# converti les arguments en float
# billet = float(billet)
# prix   = float(prix)

# notre mission
# billet, prix
# 1/ billet < prix -> pas assez
# 2/ billet == prix -> pile
# 3/ billet > prix -> ok, rendre monnaie
print('-----------')
print('-----------')

# exemples pour voir le fonctionnement de IF
# if True:
#     print("je suis dans le IF")
#     print("je suis deux fois dans le IF")
#     if True:
#         print(" je suis dans le IF qui est dans le IF")
# print("je ne suis pas dans le IF")
# if False:
#     print("je suis dans le IF")
#     print("je suis deux fois dans le IF")
#     if True:
#         print(" je suis dans le IF qui est dans le IF")
# print("je ne suis pas dans le IF")

# a = 10
# if a == 10:
#     a = a + 2
#     print(a)
#     a = a + 3
#     print(a)

# a = 10
# if a == 10:
#     print('IN')
#     if a >= 10:
#         a += 20
#         print(a)
#     a = a / 2
#     if a % 2 == 0:
#         print(a)
# print('OUT')
#
# if False:
#     print('Dans le if --- 1')
# else :
#     print('Dans le if --- 2')
#
# if False:
#     print('Dans le if --- 1')
# else :
#     print('Dans le if --- 2')


# a = 11
# if a % 2 == 0:
#     print('A est pair')
# elif a % 2 == 1:
# 	print('A est impair')

_, arguments, *_ = sys.argv
a = arguments
if type(a) != int or float:
	print(f'{a} n\'est ni un chiffre ni un nombre')
else:
    a = float(a)
    if a % 2 == 0 and a % 5 == 0:
        print(f'{a} est pair et multiple de 5')
    elif a % 2 == 0 or a % 5 == 0:
        if a % 2 == 0:
            print(f'{a} est pair')
        else:
            print(f'{a} est multiple de 5')
    else:
        print(f'{a} n\'est ni multiple de 5 ni pair')