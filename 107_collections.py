# Collections : Tableaux, listes, tuples ...
# Tuples () : immutable -> non modifiable
# Liste [] : mutable -> modifiable : rajouter/supprimer des éléments ou les modifier
# Contient plusieurs éléments

# a = 5
# b = "Toto"

# --------------- Tuples -----------------
# personnes = ("Mélanie", "Jean", "Martin", "Alice")
# print(personnes[0])
# print(len(personnes))
# print(personnes)

# for i in range(0, len(personnes)):
#     print(personnes[i])

# print(personnes[-1])

# for i in personnes:
#     print(i)
#     print(len(i))
#     print(i[0])

# (0, 1, 2, 3, 4,)
# valeurs = range(0,5)
# print(valeurs[-1])

# ----------------Listes------------------
"""
personnes = ["Mélanie", "Jean", "Martin", "Alice"]
nouvelle_personne = "David"

# print(personnes)
# del personnes[1]
#
# print(personnes)
#
# def affichier_personnes(c):
#     for i in c:
#         print(i)
#
# affichier_personnes(personnes)

def modifier_valeur(a):
    # a = 10
    # print(a)
    a[0] = 10

test = [1, 2, 3, 4]
print(test)
modifier_valeur(test)
print(test)
#modifier_valeur(test)
#print(test)
"""

# ---------------- Fonctions et tuples ----------------
"""
def obtenir_informations():
     return "Mélanie", 37, 1.60


def afficher_informations(nom, age, taille):
    print(f"Informations: Nom : {nom}, age : {age}, taille: {taille}")



# infos = obtenir_informations()
# print("nom: " + infos[0])
# print("age: " +  str(infos[1]))
# print("taille: " + str((infos[2])))

# nom, age, taille = obtenir_informations()
# afficher_informations(nom, age, taille)

infos = obtenir_informations()
afficher_informations(*infos)  # unpack tuple
"""

# ---------------- Slices ---------------

personnes = ("Mélanie", "Jean", "Martin", "Alice", "Pierre", "Paul")

# [start:stop:step]

# for i in personnes[::-1]:
#     print(i)

nom = " Jean"
print(nom[::-1])

