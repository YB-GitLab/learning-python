import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
# next_checkpoint_x: x position of the next check point
# next_checkpoint_y: y position of the next check point
# next_checkpoint_dist: distance to the next checkpoint
# next_checkpoint_angle: angle between your pod orientation and the direction of the next checkpoint
# Write an action using print
# To debug: print("Debug messages...", file=sys.stderr, flush=True)
# You have to output the target position
# followed by the power (0 <= thrust <= 100)
# i.e.: "x y thrust"
# x, y, next_checkpoint_x, next_checkpoint_y, next_checkpoint_dist, next_checkpoint_angle = [int(i) for i in input().split()]
#   opponent_x, opponent_y = [int(i) for i in input().split()]
# print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " " + str(thrust))


while True:
    x, y, next_checkpoint_x, next_checkpoint_y, next_checkpoint_dist, next_checkpoint_angle = [int(i) for i in
                                                                                               input().split()]
    opponent_x, opponent_y = [int(i) for i in input().split()]

    # ---------------------------------------------------------- #

    thrust = 0
    count_game = 0
    boost_count = 3
    booster = False

    if count_game == 0:
        thrust = 100
    else:
        count_game += 1

    # if booster == True and count_game > 0:
    #     count_game -= 1

    # else:

    if next_checkpoint_dist < 600:
        if not -90 < next_checkpoint_angle < 90:
            thrust = 0
        elif not -75 < next_checkpoint_angle < 75:
            thrust = 10
        # elif not -50 < next_checkpoint_angle < 50:
        #     thrust = 15
        # elif not -25 < next_checkpoint_angle < 25:
        #     thrust = 20
        else:
            thrust = 20

    if next_checkpoint_dist < 1000:
        if not -90 < next_checkpoint_angle < 90:
            thrust = 0
        elif not -75 < next_checkpoint_angle < 75:
            thrust = 15
        # elif not -50 < next_checkpoint_angle < 50:
        #     thrust = 20
        # elif not -25 < next_checkpoint_angle < 25:
        #     thrust = 30
        else:
            thrust = 40

    if next_checkpoint_dist < 2000:
        if not -90 < next_checkpoint_angle < 90:
            thrust = 0
        elif not -75 < next_checkpoint_angle < 75:
            thrust = 20
        # elif not -50 < next_checkpoint_angle < 50:
        #     thrust = 30
        # elif not -25 < next_checkpoint_angle < 25:
        #     thrust = 45
        else:
            thrust = 50

    if next_checkpoint_dist < 3000:
        if not -90 < next_checkpoint_angle < 90:
            thrust = 0
        elif not -75 < next_checkpoint_angle < 75:
            thrust = 30
        # elif not -50 < next_checkpoint_angle < 50:
        #     thrust = 45
        # elif not -25 < next_checkpoint_angle < 25:
        #     thrust = 60
        else:
            thrust = 70

    if next_checkpoint_dist < 5500:
        if not -90 < next_checkpoint_angle < 90:
            thrust = 0
        elif not -75 < next_checkpoint_angle < 75:
            thrust = 45
        # elif not -50 < next_checkpoint_angle < 50:
        #     thrust = 60
        # elif not -25 < next_checkpoint_angle < 25:
        #     thrust = 80
        else:
            thrust = 85

    if next_checkpoint_dist > 5500:
        if not -90 < next_checkpoint_angle < 90:
            thrust = 0
        elif not -75 < next_checkpoint_angle < 75:
            thrust = 65
        # elif not -50 < next_checkpoint_angle < 50:
        #     thrust = 75
        # elif not -25 < next_checkpoint_angle < 25:
        #     thrust = 90
        else:
            if boost_count != 0:
                thrust = 'BOOST'
                if boost_count > 0:
                    boost_count -= 1
            else:
                thrust = 100

    # ---------------------------------------------------------- #

    print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " " + str(thrust))


