## Pizza V1
# Objectifs : pratiquer sur les listes
#     - listes
#     - Tuples
#     - Faire du tri
#     - Slices
#
# def tri_personnalise(e):
#     return len(e)  # ça va trier par le nombre de caractère


def afficher(collection, nb_pizza=-1):
        # afficher les pizzas, 1 pizza par ligne
    # collection.sort(reverse=True, key=tri_personnalise)  # permet de personnailsé la tri de la liste (ici par ordre du nb de caractères et une inversion de l'ordre
    print(f"----- LISTE DES PIZZAS ({len(collection)}) -----")
    c = collection
    if c == ():
        print("----- AUCUNE PIZZA -----")
        return
    if not nb_pizza == -1:
        c = collection[:nb_pizza]
        for each in c:
            print(each)
    else:
        for each in collection:
            print(each)

    premiere_pizza = collection[0]
    derniere_pizza = collection[-1]
    template = (f"la première pizza est : {premiere_pizza}\nLa dernière pizza est : {derniere_pizza}")
    print(template)


def ajouter_pizza_utilisateur(collection):
    pizza_ajout = input("Quelle pizza ajouté ? ")
    if pizza_exist(pizza_ajout, collection) == True:
        print("\nCette pizza existe déjà")
        return ajouter_pizza_utilisateur(collection)
    elif pizza_ajout == "":
        print("\nVeuillez entrer un nom")
        return ajouter_pizza_utilisateur(collection)

    # print(pizzas_update)
    else:
        collection.append(pizza_ajout.title())
    # print(pizzas_update)

def pizza_exist(test_presence, collection):
    if test_presence in collection:
        return True


pizzas_list = ["4 fromages", "Végétarienne", "Napolitaine", "Calzone"]
vide = ()
ajouter_pizza_utilisateur(pizzas_list)
afficher(pizzas_list, 3)
