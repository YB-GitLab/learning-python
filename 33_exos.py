# Tuple, Unpacking et List Slicing

# Exercice 1 - Il est possible de faire de l'unpacking avec les listes aussi

un_tuple = (1, 2, 'Hey')
une_liste = [1, 2, 'Hey']

a, b, c = un_tuple
print(a, b, c) # -> 1, 2, Hey

x, y, z = une_liste
print(x, y, z) # -> 1, 2, Hey

print('------')
# Exercice 2 - Unpacking classique

a, b, c, d = ['A', 'B', 'C', 'D']
print(a, b, c, d)

print('----')

# Exercice 3 - Premier, Reste (liste slicing et indexing)

ma_liste = [1, 2, 3, 4, 5]
premier, reste = ma_liste[0], ma_liste[1:]
print('Premier -> ', premier)
print('Le reste ->', reste)

print('-----')
# Exercice 4 - Premier, Reste (star unpacking)

ma_liste = [1, 2, 3, 4, 5]

premier, *reste = ma_liste
print('premier -> ', premier)
print('dernier -> ', reste)

print('-----')
# Exercice 5 - Reste, Dernier (liste slicing et indexing)

ma_liste = [1, 2, 3, 4, 5]

reste, dernier = ma_liste[0:4], ma_liste[-1]
# reste, dernier = ma_liste[:-1], ma_liste[-1]
print('reste -> ', reste)
print('dernier -> ', dernier)

print('----')
# Exercice 6 - Premier, Reste, Dernier (star unpacking)

ma_liste = [1, 2, 3, 4, 5]

*reste, dernier = ma_liste
print('reste -> ', reste)
print('dernier -> ', dernier)

# Exercice 7 - Premier, Avant-Dernier, Dernier (liste slicing et indexing)

ma_liste = [1, 2, 3, 4, 5]

premier, reste, dernier = ma_liste[0], ma_liste[1:-1], ma_liste[-1]
print('premier -> ', premier)
print('reste -> ', reste)
print('dernier ->', dernier)
print('-----')

# Exercice 8 -  Premier, Reste, Dernier (star unpacking)

ma_liste = [1, 2, 3, 4, 5]

premier, *reste, dernier = ma_liste
print('premier -> ', premier)
print('reste -> ', reste)
print('dernier -> ', dernier)
print('-----')

# Exercice 9 - Premier, Reste, Avant-Dernier, Dernier (liste slicing et indexing)

ma_liste = [1, 2, 3, 4, 5, 6, 7, 8]

premier, reste, avant_dernier, dernier = ma_liste[0], ma_liste[1:-2], ma_liste[-2], ma_liste[-1]
print('premier ->', premier)
print('reste ->', reste)
print('avant-dernier ->', avant_dernier)
print('dernier ->', dernier)
print('-----')

# Exercice 10 -  Premier, Reste, Avant-Dernier, Dernier (star unpacking)

ma_liste = [1, 2, 3, 4, 5, 6, 7, 8]

premier, *reste, avant_dernier, dernier = ma_liste
print('premier ->', premier)
print('reste ->', reste)
print('avant-dernier', avant_dernier)
print('dernier ->', dernier)


