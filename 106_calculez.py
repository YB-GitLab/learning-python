## Pratiquer sur les notions déjà vues
# - boucles, variables, conditions
# - autonomie
# - Opérateurs
# - Nombres aléatoires
#       - interpréter

import random

NOMBRE_MIN = 1
NOMBRE_MAX = 10
nb_coup_init = 2
nb_point_init = 0


def poser_question(nb_coup, nb_point):
    while nb_coup > 0:
        print("Question N°", nb_coup_init - nb_coup + 1, " sur ", nb_coup_init)
        a = random.randint(NOMBRE_MIN, NOMBRE_MAX)
        b = random.randint(NOMBRE_MIN, NOMBRE_MAX)
        o = random.randint(0, 1)
        operateur_str = "+"
        if o == 1:
            operateur_str = "*"
        reponse_str = input(f"Calculez : {a} {operateur_str} {b} = ")
        reponse_int = int(reponse_str)
        calcul = a + b
        if o == 1:
            calcul = a * b
        if int(reponse_int) == calcul:
            print("\nRéponse correcte\n")
            nb_coup = nb_coup - 1
            nb_point = nb_point + 1
            return poser_question(nb_coup, nb_point)
        else:
            print("\nRéponse incorrecte\n")
            nb_coup = nb_coup - 1
            nb_point = nb_point + 0
            return poser_question(nb_coup, nb_point)
    return nb_point

def afficher_resultat(resultat, nb_coup):
    note_max = resultat == nb_coup
    note_min = resultat == 0
    note_moy_haute = resultat > int(nb_coup // 2)
    note_moy_basse = resultat < int(nb_coup // 2)
    if note_max:
        appreciation = "Le meilleur score !"
    elif note_min:
        appreciation = "Révisez !"
    elif note_moy_haute:
        appreciation = "Pas mal !"
    else:
        appreciation = "Peut mieux faire !"
    template = (f"Vous avez obtenu : {resultat_obtenu} / {nb_coup}\n{appreciation}")
    print(template)


resultat_obtenu = poser_question(nb_coup_init, nb_point_init)
afficher_resultat(resultat_obtenu, nb_coup_init)


