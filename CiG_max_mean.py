import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

i = 3
thrust = 100
# game loop
while True:
    # next_checkpoint_x: x position of the next check point
    # next_checkpoint_y: y position of the next check point
    # next_checkpoint_dist: distance to the next checkpoint
    # next_checkpoint_angle: angle between your pod orientation and the direction of the next checkpoint
    x, y, next_checkpoint_x, next_checkpoint_y, next_checkpoint_dist, next_checkpoint_angle = [int(i) for i in
                                                                                               input().split()]
    opponent_x, opponent_y = [int(i) for i in input().split()]
    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr, flush=True)
    boost = True
    if i > 0 and 5500 < next_checkpoint_dist < 8500 and -30 < next_checkpoint_angle < 30:
        if boost == True and thrust == 100:
            thrust = "BOOST"
            boost = False
        else:
            thrust = "BOOST"
        i -= 1
    else:
        if next_checkpoint_dist > 3500:
            if not -90 < next_checkpoint_angle < 90:
                thrust = 0
            else:
                thrust = 100
        elif 2250 < next_checkpoint_dist < 3500:
            if not -90 < next_checkpoint_angle < 90:
                thrust = 0
            else:
                if thrust == 100:
                    thrust = 90
        elif 1350 < next_checkpoint_dist < 2250:
            if not -90 < next_checkpoint_angle < 90:
                thrust = 0
            elif not -75 < next_checkpoint_angle < 75:
                thrust = 55
            elif not -50 < next_checkpoint_angle < 50:
                thrust = 70
            else:
                thrust = 80
        elif next_checkpoint_dist < 1350:
            thrust = 70
        elif next_checkpoint_dist < 900:
            thrust = 55
        elif next_checkpoint_dist < 600:
            thrust = 35
        elif next_checkpoint_dist < 300:
            thrust = 15

        else:
            thrust = 0

        # You have to output the target position
    # followed by the power (0 <= thrust <= 100)
    # i.e.: "x y thrust"

    print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " " + str(thrust))


