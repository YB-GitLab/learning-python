# Les opérateurs de comparaisons

a = 0 == 0
b = 3 > 2
c = 2 >= 2
d = 3 != 2

print(a, b, c, d)

print('--------')
# Les imprécisions de Float

a = 3 * 2 / 5.3
b = 3 * (2 / 5.3)

print(a)
print(b)
print(a == b)
# retourne False

print('-----------------')
# Solution d'imprécision avec epsilon

epsilon = 0.00000000001
print(-epsilon < a - b < epsilon)

