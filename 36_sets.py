# les Sets

ensemble = set()
print(type(ensemble))
print(bool(ensemble))
print(len(ensemble))
print('----')

# on peut transformer une liste en set
repas = ['salade', 'salade', 'sushis', 'sushis', 'sushis']
menu = set(repas)
print(menu)
print('-----')

# utilisation de l'opérateur «in» pour la recherche
print('fruits' in menu)
print('----')

# ajout d'un élément
menu.add('fruits')
print(menu)
print('-----')

# supprimer d'un élément
menu.remove('fruits')
print(menu)

menu.add('fruits')
menu.add('fruits')
menu.add('fruits')
menu.add('fruits')
menu.add('fruits')
print(menu)

