# for inline (sur une seule ligne)
# any
# sum
# zip


# 1 ---- for inline
class Pizza:
    def __init__(self, nom, prix):
        self.nom = nom
        self.prix = prix

pizzas = [
    Pizza("Calzone", 8),
    Pizza("4 fromages", 9.5),
    Pizza("Hawai", 10)
]

# noms_pizzas = []
# for pizza in pizzas:
#     noms_pizzas.append(pizza.nom)

noms_pizzas = [pizza.nom for pizza in pizzas]  # For sur une seule ligne
# noms_pizzas = [pizza.nom for pizza in pizzas if pizza.prix >= 10]   # for sur une seule ligne avec des conditions
print(noms_pizzas)

# 2 ---- any
l = [i.prix > 10 for i in pizzas]
print(l)
pizza_chere_existe = any([i.prix > 10 for i in pizzas])
print(pizza_chere_existe)


# 3 ---- sum
l = [1 for i in pizzas if i.prix > 9]
print(l)
nb_pizza_cheres = sum([1 for i in pizzas if i.prix > 9])  # ajoute 1 pour chaque pizza > 9€ trouvé
print(nb_pizza_cheres)

print(sum([10, 12, 3]))


# 4 ---- zip

pizzas_nom = ("4 fromages", "Calzone", "Hawai")
pizzas_prix = (10.5, 11, 8)

noms_et_prix = list(zip(pizzas_nom, pizzas_prix))

for (nom, prix) in noms_et_prix:
    print(f"{nom} - {prix}€")

unzipped = list(zip(*noms_et_prix))
print(unzipped)
