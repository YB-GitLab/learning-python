# la méthode .format()

# mais avant revoyons le f-string
un_mot = 'nia nia' # --> fonctionne car déclarer avant le f-string

chaine = f'bla bla bla {un_mot}'
#un_mot = 'nia nia' # --> ne fonctionne pas car déclarer après le f-string
print(chaine)
# on ne peut pas anticiper un besoin avec f-string

print('---------')
# la méthode .format()
msg = 'Bienvenue {nom}'
print(msg)
print(msg.format(nom='Rémi'))
print('--------')

# Méthode .format() à vide
# ce sont en faît des templates, et non des chaînes
template = "bla {} bla {} bla {} bla"
print(template.format('A', 'B', 'C'))
print('---------')

# méthode format indexé
template = "bla {0} bla {1} bla {2}"
print(template.format('A', 'B', 'C'))
# on peut modifié l'ordre à la convenance
template = "bla {0} bla {2} bla {1}" # <-- ordre modifié
print(template.format('A', 'B', 'C'))
print('--------')

#méthode format nommé -> on va nommé à l'intérieur des {} ex : {name1}
template = "bla {name1} bla {name2} bla {name3} bla" # <-- ordre modifié
print(template.format(name1='A', name2 = 'B', name3=12))

