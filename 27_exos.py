# Techniques de Formatage et String Methods

# Exercice 1 - Commande dans un restaurant (f-string)
# Que va afficher le code ci-dessous?
plat_choix1 = 'POULET'
plat_choix2 = 'POISSON'

boisson_choix1 = 'EAU PÉTILLANTE'
boisson_choix2 = 'EAU MINERALE'

commande = f'Je prendrai le {plat_choix1} avec comme boisson l\'{boisson_choix1} s\'il vous plait' # poulet et eau pétillante
print(commande)
print('-----')
# Ensuite modifier le code pour commander du Poisson et de l'Eau Minéral.
commande = f'Je prendras le {plat_choix2} avec comme boisson l\'{boisson_choix2} s\'il vous plait'
print(commande)
print('--------')
# Exercice 2 - Mettre une string en lettres minuscule (dans une f-string)
commande = f'Je prendrai le {plat_choix1.lower()} avec comme boisson l\'{boisson_choix1.lower()} s\'il vous plait'
print(commande)
print('-------')
# Exercice 3 - Commande dans un restaurant (template & format)
# Que va afficher le code ci-dessous?
plat_choix1 = 'POULET'
plat_choix2 = 'POISSON'
boisson_choix1 = 'EAU MINERAL'
boisson_choix2 = 'EAU PETILLANTE'

template_commande = 'Je prendrai le {plat} avec comme boisson de l\'{boisson} s\'il vous plait'
ma_commande = template_commande.format(plat=plat_choix1, boisson=boisson_choix2)
print(ma_commande) # POULET EAU PETILLANTE
print('--------')
#Ensuite modifier le code pour commander du Poisson et de l'Eau Pétillante
ma_commande = template_commande.format(plat=plat_choix2, boisson=boisson_choix2)
print(ma_commande)
print('--------')
# Exercice 4 - Mettre la première lettre en majuscule et le reste en minuscule
# Que va afficher le code ci-dessous?
# rentre une valeur à l'objet variable str
prénom = 'RÉMI'
# modifie la première lettre de la variable prénom en Majuscule
première_lettre = prénom[0].upper()
# modifie le reste de la variable prénom en minuscule
reste_du_prénom = prénom[1:].lower()
# Créer une variable str avec l'addition des valeurs des 2 variables
prénom_première_lettre_majuscule = première_lettre + reste_du_prénom
# va afficher la variable
print(prénom_première_lettre_majuscule) # va afficher Rémi
print('--------')
# Exercice 5 - Une méthode équivalente pour obtenir un prénom en initiale majuscule
prénom = 'RÉMI'
ma_solution = prénom.title()
print(ma_solution)
print('---------')
# Exercice 6⭐ Combiner les méthodes
#Que va afficher le code ci-dessous?
prénom = 'RÉMI'
première_lettre = prénom[0].lower()
reste_du_prénom = prénom[1:].upper()
prénom_première_lettre_majuscule = première_lettre + reste_du_prénom
print(prénom_première_lettre_majuscule) # va afficher rÉMI
print('-------')
# Il est possible d'obtenir le même résultat en utilisant à la suite les méthodes découvertes précédemment
prénom = 'RÉMI'
ma_solution = prénom.title().swapcase()
print(ma_solution)
print('-------')
# Exercice 7 - Supprimez les espaces surperflus
# supprimer les espaces dans l'adresse mail
email_donné_par_lutilisateur = '      une_Adresse_Mail@Mail.COM      '
email_de_BDD = 'une_adresse_mail@mail.com'
# ici on met tout en minuscule car les mails sont insensibles à la casse et on supprime les espaces en trop
email_donné_par_lutilisateur = email_donné_par_lutilisateur.lower().strip()
# ici on compare si ils sont identiques
email_identiques = email_donné_par_lutilisateur == email_de_BDD
print('emails identiques ?', email_identiques) # cette ligne doit afficher True


