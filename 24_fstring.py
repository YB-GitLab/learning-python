# les f-string

prénom = 'Rémi'
msg_de_bienvenue = f'salut {prénom}'

print(msg_de_bienvenue)

print('----------')
# test avec int et float
type_int = '1'
type_float = '1.0'

test_type_int = f'type int {type_int}'
test_type_float = f'type float {type_float}'

print(test_type_int + ' || ' + test_type_float)

print('------------')

addition = f'la somme de deux et deux est {2 + 2}'
print(addition)

print('-----------')
# dans des epressions

var = 34
msg = f'Le type de {var} est {type(var)}'

print(msg)

