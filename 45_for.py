# Les boucles For
import sys

#nb_repetition = int(sys.argv[1])

#collection = [ 'a', 'b', 'c'] * nb_repetition
#print(collection)

#for iter_var in collection:
#    print(iter_var)


print('-' * 15)
# Les itérables

# iter(10) Pas itérable
# iter(10.0) Pas itérable
# iter(bool) Pas itérable
iter('str')
iter(['list1', 'list2'])
iter(set())
iter(dict())

print('#' * 15)
# les itérators
iterable = ['Hey', 'Sun', 'Moon']
iterator = iter(iterable)
print(iterator, type(iterator))
objet = next(iterator)
print(objet)
objet = next(iterator)
print(objet)

print('#' * 15)
# Bonnes pratiques avec les boucles For

# nommer correctement
factures = ['Fac1', 'Fac2', 'Fac3', 'Fac4'] # ici c'est une liste de factures soit -- > factures (au PLURIEL)
for facture in factures:# ici l'iterator sera facture (au SINGULIER)
    pass
    # facture.payer()

print('#' * 15)

# enumerate()
factures = ['Fac1', 'Fac2', 'Fac3', 'Fac4']
for facture in enumerate(factures):
    print(facture, type(facture))

print('#' * 15)
# on peut directement mettre un compteur
factures = ['Fac1', 'Fac2', 'Fac3', 'Fac4']

for i, facture in enumerate(factures):
    print(i, facture)

print('#' * 21)
# Travailler avec zip()
symboles = ['+', '-', '*', '/']
operations = ['addition', 'soustraction',
              'murtiplication', 'division']
template = 'Le symbole de l\'opération {op} est {sym}'

for symbole, operations in zip(symboles, operations):
    message = template.format(op=operations, sym=symbole)
    print(message)

print('#' * 21)

## Les Dictionnaires avec FOR

operations = {'addition'      :'+',
              'soustraction'  :'-',
              'mulitplication':'*',
              'division'      :'/'}

message = 'L\'opération {o} est définie par le symbole {s}'

for objet in operations.items():
    print(objet, type(objet))

for k, v in operations.items():
    print(k, v)

for k in operations.keys():
    print(k)

for v in operations.values():
    print(v)

print('#' * 21)

# Travailler avec range()

#for i in range(start, end, step):
#	print(i)

# créer une liste de nombre allant de 0 à 99
liste_de_nombres = list(range(0, 100))
print(liste_de_nombres)

# afficher une suite décroissante avec reversed
for i in reversed(range(5, 10+1)):
    print(i)

# afficher une suite décroissante avec step -1
for i in range(10, 0, -1):
    print(i)

print('#' * 21)

symboles = ['+', '-', '*', '/']
operations = ['addition', 'soustraction',
              'murtiplication', 'division']
ajouts = ['a', 'b', 'c', 'd']

template = 'Le symbole de l\'opération {op} est {sym} et l\'ajout est {aj}'

for symbole, operations, ajout in zip(symboles, operations, ajouts):
    message = template.format(op=operations, sym=symbole, aj=ajout)
    print(message)

print('#' * 21)