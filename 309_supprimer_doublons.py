# Supprimer les doublons (remove duplicate)
#
# 1 - Comparaison linéaire (n^2)
# 2 - Tri + comparaison successive (n.Log(n))
# 3 - Dictionnaire (hash map) (n)
#
# exists
# set

from utils import generate_random_list
l = generate_random_list(210000, 0, 20000)
# print(l)

# l = [7, 1, 10, 7, 9, 5, 6, 1, 1, 8, 8, 10, 3, 1, 8, 1, 5, 4, 8, 1]

# 1 - Comparaison linéaire

def remove_duplicate_linear(l):
    u = []
    for e in l:
        found = False
        for ue in u:
            if e == ue:
                # on a trouvé l'élément : duplicate
                found = True
                break
        if not found:
            u.append(e)
    return u

#print(remove_duplicate_linear(l))

# 2 - Tri + Comparaison successive

def remove_duplicate_sort(l):
    l.sort()
    u = []
    for i in range(len(l)):
        if l[i-1] != l[i]:
            u.append(l[i])

    return u

#print(remove_duplicate_sort(l))

# 3 - Dictionnaire (hash map)

def remove_duplicate_hash_map(l):  # le plus rapide
    d = {}
    for e in l:
        v = d.get(e)
        if v:
            v += 1
            d[e] = v
        else:
            d[e] = 1
    r = []
    for key in d:
        r.append(key)
    for key, value in d.items():
        print(key, "->", "x"+str(value))

    return r
    print("END")


print(remove_duplicate_hash_map(l))
