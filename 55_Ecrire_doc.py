def recherche_minimum(liste):
    """ 'Description de la fonction'
        Permet la recherche de la valeur minimum d'une liste. La fonction est compatible avec tout type qui supporte
         l'opérateur chevron strictement inférieur(<). Car c'est < qui est utilisé pour rechercher le minimum

    Args:
        liste (list): la liste dans laquelle on cherche le minimum
            (Peut-etre vide, dans ce cas on retournera None)

        parameter (parameter_type): explication du paramètre
            cette ligne pour continuer mon explication car elle est longue
            des 'Détails'
        parameter2 (parameter2_type): explication du param
            Avec de longues explictaitons et des détails
            et d'autres
            (valeur par défaut : 23)

    Returns:
        minimum(): La valeur minimum trouvé dans la liste, dans l'ordre défini par
            l'opérateur chevron strictement inférieur (<)

        return_value1 (return_value1_type): explication de la valeur de retour
            Et continue l'explication
        return_value2 (return_value2_type): explication de la valeur de retour
            Et continue l'explication
    """
    # Si la liste est vide on reourne None pour éviter le crash
    if not liste:
        return None
    # Si la liste est non vide alors ...
    minimum, *reste = liste
    for each in reste:
        if each < minimum:
            minimum = each
        return minimum


def recherche_maximum(liste):
    if not liste:
        return None
    maximum, *reste_liste = liste
    for each in reste_liste:
        if each > maximum:
            maximum = each
    return maximum


def recherche_extremum(liste, min_ou_max):
    """

    :param liste:
    :param min_ou_max:
    :return:
    """
    if min_ou_max.lower() == 'min':
        extremum = recherche_minimum(liste)
    elif min_ou_max.lower() == 'max':
        extremum = recherche_maximum(liste)
    else:
        # Mode non supporté (min ou max obligatoire), donc on retourne None
        extremum = None
    return extremum


Liste_test = [-5, 4, 3, -15]
print(recherche_minimum(Liste_test))
print(recherche_maximum(Liste_test))
print(recherche_extremum(Liste_test, 'min'))
print(recherche_extremum(Liste_test, 'max'))
