# Opérations sur les strings
# Exercice 1 - Concaténation et saut de lignes

message_1 = "J\'espère que ce cours te plait!"
message_2 = "j\'y ai mis tout mon coeur."

à_afficher = message_1 + '\n' + message_2

print(à_afficher)

print('-----')

# Exercice 2 - Concaténation et insérer des espaces
# Nous souhaitons afficher le message "Hello World!"
texte1 = "Hello"
texte2 = "World"
texte3 = "!"

à_afficher = texte1 + ' ' + texte2 + texte3
print(à_afficher)

print('-------')

# Exercice 3 - Corriger une Type Error
# Le code ci-dessous ne s'exécute pas correctement, s'il vous plait, corrigez le.

message = 'Ce produit coûte '
prix = 200
devise = ' euros'

à_afficher = message + str(prix) + devise
print(à_afficher)

print('-----------')

# Exercice 4 - Encore plus de TypeError

à_afficher = str("1") + ' ' + str(2) + ' ' + str(3) + ' Soleil'
print(à_afficher)

print('----------')

# Exercice 5 - Afficher de beaux carrés

ligne = '*' * 5 + '\n'
carré = ligne * 5
print(carré)
# Modifier le code pour transformer ce qui s'affiche en un carré de trois lignes par trois colonnes.
ligne = '*' * 3 + '\n'
carré = ligne * 3
print(carré)
# dix lignes pour dix colonnes?
ligne = '*' * 10 + '\n'
carré = ligne * 10
print(carré)
# dix lignes pour quatre colonnes?
ligne = '*' * 10 + '\n'
carré = ligne * 4
print(carré)

print('-----------')

# Exercice 6 - Ce code permet d'afficher un joli sapin
# vous constatez qu'il lui manque un tronc.'
# exercice - S'il vous plaît ajouter lui un tronc.

ligne1 = ' ' * 2 + '*' + ' ' * 2 + '\n'
ligne2 = ' ' + '*' * 3 + ' ' + '\n'
ligne3 = '*' * 5 + '\n'
bout_de_sapin = ligne1 + ligne2 + ligne3
tronc_de_sapin = ' ' * 2 + '|' + ' ' * 2 + '\n'


print(bout_de_sapin + tronc_de_sapin)


# Exercice 7 - Un sapin encore plus grand
# Essayez de modifier le code pour que maintenant les bouts de sapin soient sur quatre lignes.

scape = ' '
étoile = '*'
tronc = '| |'
ligne1 = scape * 3 + étoile + scape * 3 + '\n'
ligne2 = scape * 2 + étoile * 3 + scape * 2 + '\n'
ligne3 = scape * 1 + étoile * 5 + scape * 2 + '\n'
ligne4 = étoile * 7 + '\n'
ligne5 = scape * 2 + tronc
bout_de_sapin = ligne1 + ligne2 + ligne3 + ligne4 + ligne5

print(bout_de_sapin)

print('-------')

count = 0
étoile = '*'
for i in range(5):
    count = count + 1
    print(étoile * count)

print('-----------')

i = 0
x = 6
count_étoile = -1
count_scape = int(x // 2 + x)
count_scape_tronc = (x // 2 + x - 2)
étoile = '*'
scape = ' '
tronc = '| |'

for i in range(x):
    count_scape = count_scape - 1
    count_étoile = count_étoile + 2
    print(scape * count_scape + étoile * count_étoile + scape * count_scape)
print(scape * count_scape_tronc + tronc)

print('-----')

i = 0
x = 15

count_étoile = -1
count_scape = int(x // 2 + x)
count_scape_first = int(x // 2 + x - 1)
count_scape_corps = (x - 2)
étoile = '*'
scape = ' '

print(scape * count_scape_first + étoile)
for i in range(x // 2):
    count_scape = count_scape - 3
    count_étoile = count_étoile + 6
    print(scape * count_scape + étoile * count_étoile + scape * count_scape)
for j in range(x // 2):
    print(scape * (count_scape + 2) + étoile * (count_étoile - 4))
