# Les Opérateurs d'inclusion

# Exercice 1 - Opérateur d'inclusion

substring = 'Trop grande ...'
string = 'Trop petite.'
print(substring in string) # false

print('---------')
# Exercice 2 - Emplacement de la substring

substring = "Rémi"

strings1 = 'Je m\'appelle Rémi' # True
strings2 = 'Rémi c\'est mon nom' # True
strings3 = 'Moi c\'est Rémi et toi ?' # True
strings4 = 'Dans cette string je ne dirai pas mon nom' # False

print(substring in strings1)
print(substring in strings2)
print(substring in strings3)
print(substring in strings4)

print('--------')

#Exercice 3 - Cas d'usage

nom_fichier_1 = 'Mon film préféré.mp4'
nom_fichier_2 = 'Ma musique préférée.mp3'
nom_fichier_3 = 'Ma photo préférée.jpg'

print('.mp3' in nom_fichier_1) # False
print('.mp3' in nom_fichier_2) # True
print('.mp3' in nom_fichier_3) # False

print('-----------')

# Exercice 4 - Détail auquel il faut faire attention

# une erreur s'est glissé dans le code
# Quelle est l'erreur qui a été commise dans ce bout de code?
# Pouvez vous la corriger?
# Quelle leçon en tirez vous?

nom_fichier_1 = 'lire les fichiers mp3.mp4'
nom_fichier_2 = 'musique.mp3'
nom_fichier_3 = 'photo lecteur mp3.jpg'

print('.mp3' in nom_fichier_1)
print('.mp3' in nom_fichier_2)
print('.mp3' in nom_fichier_3)