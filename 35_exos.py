# Exercices sur les dictionnaires

# Exercice 1 - Manipulez les dictionnaires en Python

menu = {'Plat - Poisson': 100,
        'Plat - Végé': 90,
        'Plat - Burger': 130}

print(menu)
# afficher le prix de plat poisson
print(f'Le plat de poisson vaut : {menu["Plat - Poisson"]} euros')
# modifier le prix du plat burger à 150 puis afficher le menu
menu['Plat - Burger'] = 150
print(menu)
# Ajouter un nouveau plat, le double burger à 290, puis afficher le menu
menu['Plat - Double-burger'] = 290
print(menu)
# supprimer le plat burger
del menu['Plat - Burger']
print(menu)
print('----')
# Exercice 2 - Opérateur in
menu = {'plat - poisson': 100, 'plat - végé': 90, 'plat - double burger': 290}
print('plat - Burger' in menu) # False
print(100 in menu) # False
print('plat - végé' in menu) # True
print(290 in menu) # False
print('----')
# Exercice 3 - Longueur d'un dictionnaire
menu = {'plat - poisson': 100, 'plat - végé': 90, 'plat - double burger': 290}
n_plats = len(menu)
print(f'Nous avons {n_plats} au menu ce soir')
print('-----')
# Exercice 4 - Contourner la KeyError
menu = {'plat - poisson': 100, 'plat - végé': 90, 'plat - double burger': 290}
plat = menu.get('Plat - Citrouille')
print(plat)
print('------')
# Exercice 5 - Remplacer la valeur par défaut retourner par get()
menu = {'plat - poisson': 100, 'plat - végé': 90, 'plat - double burger': 290}
plat = menu.get('Plat - Citrouille', 'Nous n\'avons pas ce plat')
print(plat)
print('------')
# Exercice 6 - Suppression et KeyError
menu = {'plat - poisson': 100, 'plat - végé': 90, 'plat - double burger': 290}
# del menu['Plat - Citrouille'] # erreur
menu.pop('Plat - Citrouille', -1)
print('-------')
# Exercice 7 - Récupérer une liste de couples clé-valeur
menu = {'plat - poisson': 100, 'plat - végé': 90, 'plat - double burger': 290}
couple_cle_valeur = list(menu.items())
print(couple_cle_valeur)
# Exercice 8 - Récupérer une liste de clefs.
menu = {'plat - poisson': 100, 'plat - végé': 90, 'plat - double burger': 290}
clefs = list(menu.keys())
print(clefs)
print('-----')
# Exercice 9 - Récupérer une liste de values
menu = {'plat - poisson': 100, 'plat - végé': 90, 'plat - double burger': 290}
valeurs = list(menu.values())
print(valeurs)