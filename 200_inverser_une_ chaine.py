# Inverser une chaîne de caractère (Reverse)
# deux solutions : boucle ou slice


"""
def reverse_string_boucle(str):
    r = ""
    for c in str:
        r = c + r
        # B  # à la première boucle, il va mettre dans r le "B"
        # oB  # à la seconde boucle, il va rajouter le o … il va mettre dans r "oB"
        # noB  # à la troisième boucle, il va mettre dans r "noB"
        # ainsi de suite
    return r
s = "Bonjour Toto"

print(reverse_string_boucle(s))
"""

def reverse_string_slice():
    return s[::-1]


s = "Bonjour Toto"

print(reverse_string_slice(s))