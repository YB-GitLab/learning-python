#les opérateurs booléens

# Exercice 1 - Révision de la table de vérité NOT

pas_vrai = not True # Faux
pas_faux = not False # Vrai

print(pas_vrai)
print(pas_faux)

print('---------------')

# Exercice 2 - Révision de la table de vérité OR

faux_ou_faux = False or False # Faux
faux_ou_vrai = False or True # True
vrai_ou_faux = True or False # True
vrai_ou_vrai = True or True # True

print(faux_ou_faux)
print(faux_ou_vrai)
print(vrai_ou_faux)
print(vrai_ou_vrai)

print('------------')
# Exercice 2 - Révision de la table de vérité AND

#Commencer par écrire l'expression permettant de vérité la parité, puis l'expression permettant de vérifier que le nombre est plus grand que dix.
#Ensuite combiner les deux résultats à l'aide d'une porte logique.

nombre = 16
pair = 10 % 2 == 0
plus_grand_que_dix = nombre > 10
pair_et_plus_grand_que_dix = pair and plus_grand_que_dix

print(pair_et_plus_grand_que_dix)
print('-----------')
# Exercice 5 - Inférieur à cinq ou multiple de trois
# vérifier qu'un nombre est inférieur ou bien est multiple de trois.

nombre = 6000

inférieur_à_cinq = nombre < 5
multiple_de_trois = nombre % 3 == 0
inférieur_à_cinq_ou_multiple_de_trois = inférieur_à_cinq or multiple_de_trois

print(bool(inférieur_à_cinq_ou_multiple_de_trois))

print('------------')

# Exercice 6 - Porte logique et opérandes non-booléennes (suite..)

liste_vide = []
zéro = 0
liste_vide_ou_zéro = liste_vide or zéro # Faux

print(bool(liste_vide_ou_zéro))

print('-------------')
# Exercice 7 - Porte logique et opérandes non-booléennes (suite..)

liste_remplie = ['']
zéro = 0
liste_remplie_ou_zéro = liste_remplie or zéro # Vrai
print(bool(liste_remplie_ou_zéro))

print('----------------')

# Exercice 8 — Un premier casse-tête

casse_tête = 0 or '' and [] or {} #Faux
print(bool(casse_tête))

print('-------------')
# Exercice 9 — Un second casse-tête

casse_tête = 10 % 3 and not 10 < 5 # vrai
print(bool(casse_tête))

print('-------------')
# Exercice 10 — Encore un casse-tête

casse_tête = False or True and True or False # Vrai
print(bool(casse_tête))

print('------------')

# Exercice 11 — Et un autre...

casse_tête = False or (not False and True) or True and not False # vrai
test = (not False and True)
print(bool(casse_tête))
print(bool(test))

print('----------')

casse_tête = 0 or (not False and 10) and 10 // 2 > -1 # Vrai
print(casse_tête)
